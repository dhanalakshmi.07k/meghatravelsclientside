import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnDemandshiftTypeComponent } from './on-demandshift-type.component';

describe('OnDemandshiftTypeComponent', () => {
  let component: OnDemandshiftTypeComponent;
  let fixture: ComponentFixture<OnDemandshiftTypeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnDemandshiftTypeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnDemandshiftTypeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
