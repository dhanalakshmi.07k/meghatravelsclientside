import { Component, OnInit } from '@angular/core';
import {CctransactionService} from "../../../services/cctransaction.service";
import {AssetService} from "../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import * as _ from "lodash";
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import {CorporateContractTypeService} from "../../../services/corporate-contract-type.service";
import {ExcelService} from "../../../services/excel.service";
import {PagerService} from "../../../services/pager.service";
import {LesserTransactionService} from "../../../services/lesserAssociation/lesser-transaction.service";
import {LesserAssociationService} from "../../../services/lesserAssociation/lesser-association.service";


@Component({
  selector: 'app-on-demand-corporate-contract',
  templateUrl: './on-demand-corporate-contract.component.html',
  styleUrls: ['./on-demand-corporate-contract.component.scss']
})
export class OnDemandCorporateContractComponent implements OnInit {
    corporateContractType:any= "OnDemand"
    deleteTransactionData:any
    companyAssetDropDown:any;
    bulkUpload:any={}
    public CC3List:any=[];
    public resultList:any=[];
    companyTransactionSelected:any={}
    public vechicleSelectedDetails = {};
    vechicleDropdownList:any=[];
    public vechicleTransactionSelected = "";
    public selectedVehicleItems = "";
    public companySelected = "";
    formConfigData:any={}
    transactionDate:any;
    corporateContractFields:any;
    public vechicleSelectedVehicleType = "";
    companyDropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'companySelectedForDropDown',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    vechicledropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'vehicleNumber',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
    };
    getIncialConfig(){
        this.bulkUpload.transactionStartDate=new Date().toJSON().slice(0,10);
        this.bulkUpload.transactionEndDate=new Date().toJSON().slice(0,10);
        this.getAllCompanyOfTypeCC3();
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
    }

    lesserConfiguration:any

    listData:any
    listIntermediateData:any
    tableDataObj:any=[];
    cc1ExcelDetails:any;
    uploadCc1ExcelDetails(event){
        let that=this
        let tableObjs;
        let input = event   .target;
        let reader = new FileReader();
        reader.onload = function(){
            let fileData = reader.result;
            let wb = XLSX.read(fileData, {type : 'binary'});
            wb.SheetNames.forEach(function(sheetName){
                let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs=JSON.stringify(rowObj);

                if (tableObjs == undefined || tableObjs== null){
                    return;
                }
                else{
                    console.log(JSON.stringify(rowObj))
                    that.listData=JSON.stringify(rowObj)
                    that.saveIntermediate(that.listData)
                }
            })
        };
        reader.readAsBinaryString(input.files[0])


    }
    saveIntermediate(listData){
        console.log("intermediate")
        this.listIntermediateData=listData
    }

    saveBulkDetails(arrayObjData){

        this.assetService.getAssetDetailsByVehicleId(arrayObjData['vehicleNumber'])
            .subscribe((bulkVehicleDetails: any) => {
                console.log("-0-bulkVehicleDetails=========After savee======obj=====")
                console.log(bulkVehicleDetails )
                if(bulkVehicleDetails.length!==0){
                       this.lesserAssociationService.getLesserAssociationByLesserNameAndCCType(bulkVehicleDetails[0]['lesserName'],this.corporateContractType,
                           bulkVehicleDetails[0]['vehicleType'])
                  .subscribe((lesserBulkConfig: any) => {
                      console.log("******bulk lesser config****objData*********");
                      console.log(lesserBulkConfig)
                      if(lesserBulkConfig.length!==0){
                          this.saveBulkLesserTransction(lesserBulkConfig[0],arrayObjData,bulkVehicleDetails[0]['lesserName']);

                      }
                      else{
                          alert("lesser is not avalibale for vehicle"+bulkVehicleDetails[0]['vehicleNumber'])
                      }


                  })
                }
                else{
                    alert("vehicle is not available in the system"+arrayObjData['vehicleNumber'])
                }


            })
    }

    saveBulkLesserTransction(lesserConfig,objData,lesserName){
        console.log("******finally entiredddddddddddddd?????????????????/****objData*********");
        console.log(lesserConfig);
        console.log(objData);

        let obj={}
        obj['date']=objData['date']
        obj['ratePerHr']=lesserConfig['ratePerHr']
        obj['ratePerKm']=lesserConfig['ratePerKm']
        obj['totalDaysPerHour']=objData['totalDaysPerHour']
        obj['vehicleType']=objData['mode']
        obj['bookedBy']=objData['bookedBy']
        obj['bookedBy']=objData['bookedBy']
        obj['lesserName']=lesserName
        obj['totalKms']=objData['totalKms']
        obj['vehicleNumber']=objData['vehicleNumber']
        obj['tsno']=objData['tsno']
        obj['corporateContract']=this.corporateContractType
        obj['companyTransactionSelected']=objData['company']

        if(objData['totalDaysPerHour']>=1 && objData['totalDaysPerHour']<=6 && objData['totalKms']>40){
            obj['extraKms']=objData['totalKms']-40
        }

        else if(objData['totalDaysPerHour']>6&& objData['totalKms']>80){
            obj['extraKms']=objData['totalKms']-80
        }
        else{
            obj['extraHrs']=0
        }

        if(objData['totalDaysPerHour']>=1 && objData['totalDaysPerHour']<=6){
            obj['slabRate']=lesserConfig['slabRate4hrs40kmts']
        }
        else if(objData['totalDaysPerHour']>=6){
            obj['slabRate']=lesserConfig['slabRate8hrs80kmts']
        }

        if(objData['totalDaysPerHour']>=1 && objData['totalDaysPerHour']<=6  ){
            obj['extraHrs']=objData['totalDaysPerHour']-4
        }


        else if(objData['totalDaysPerHour']>6){
            obj['extraHrs']=Math.abs(objData['totalDaysPerHour']-8)
        }
        else{
            obj['extraHrs']=0
        }
        obj['extraKmAmt']=lesserConfig['ratePerKm']*obj['extraKms']
        obj['extraHrAmt']=lesserConfig['ratePerHr']*obj['extraHrs']
        obj['tripAmt']=obj['extraKmAmt']+ obj['extraHrAmt']+ obj['slabRate'];
        this.lesserTransactionService.saveLesserTransaction(obj)
            .subscribe((data: any) => {
                console.log("-0-09-9-Bulkkkkkkkkkkkkk==========After savee======obj=====")
                console.log(data)

            });
    }



    saveCC1DetaisToDB(){
        var arrayListData = JSON.parse("[" + this.listIntermediateData + "]");
        for(let p=0;p<arrayListData[0].length;p++){
                // this.saveBulkDetails(arrayListData[0][p])
            this.vehicleCorporateContractService.getcompanyCCByCompanyAndCCTypeAndVehicleType(
                arrayListData[0][p]['company'],this.corporateContractType
                ,arrayListData[0][p]['mode'])
                .subscribe((companyCCDetails: any) =>{


                    if(companyCCDetails[0]){
                        let obj={}
                        obj['date']=arrayListData[0][p]['date']
                        obj['ratePerHr']=companyCCDetails[0]['ratePerHr']
                        obj['ratePerKm']=companyCCDetails[0]['ratePerKm']
                        obj['totalDaysPerHour']=arrayListData[0][p]['totalDaysPerHour']
                        obj['vehicleType']=arrayListData[0][p]['mode']
                        obj['bookedBy']=arrayListData[0][p]['bookedBy']
                        obj['totalKms']=arrayListData[0][p]['totalKms']
                        obj['vehicleNumber']=arrayListData[0][p]['vehicleNumber']
                        obj['tsno']=arrayListData[0][p]['tsno']
                      /*  obj['transactionStartDate']=this.bulkUpload.transactionStartDate
                        obj['transactionEndDate']=this.bulkUpload.transactionEndDate*/
                        obj['corporateContract']=this.corporateContractType
                        obj['companyTransactionSelected']=arrayListData[0][p]['company']

                        if(arrayListData[0][p]['totalDaysPerHour']>=1 && arrayListData[0][p]['totalDaysPerHour']<=6 && arrayListData[0][p]['totalKms']>40){
                            obj['extraKms']=arrayListData[0][p]['totalKms']-40
                        }

                       else if(arrayListData[0][p]['totalDaysPerHour']>6&& arrayListData[0][p]['totalKms']>80){
                            obj['extraKms']=arrayListData[0][p]['totalKms']-80
                        }
                        else{
                            obj['extraHrs']=0
                        }

                         if(arrayListData[0][p]['totalDaysPerHour']>=1 && arrayListData[0][p]['totalDaysPerHour']<=6){
                            obj['slabRate']=companyCCDetails[0]['slabRate4hrs40kmts']
                        }
                        else if(arrayListData[0][p]['totalDaysPerHour']>=6){
                            obj['slabRate']=companyCCDetails[0]['slabRate8hrs80kmts']
                        }



                        if(arrayListData[0][p]['totalDaysPerHour']>=1 && arrayListData[0][p]['totalDaysPerHour']<=6  ){
                            obj['extraHrs']=arrayListData[0][p]['totalDaysPerHour']-4
                        }


                       else if(arrayListData[0][p]['totalDaysPerHour']>6){
                            obj['extraHrs']=Math.abs(arrayListData[0][p]['totalDaysPerHour']-8)
                        }
                        else{
                            obj['extraHrs']=0
                        }
                        obj['extraKmAmt']=companyCCDetails[0]['ratePerKm']*obj['extraKms']
                        obj['extraHrAmt']=companyCCDetails[0]['ratePerHr']*obj['extraHrs']
                        obj['tripAmt']=obj['extraKmAmt']+ obj['extraHrAmt']+ obj['slabRate'];
                        this.cctransactionService.saveCCTransaction(obj)
                            .subscribe((data: any) => {
                                this.getCCTransaction(0,10);
                            });
                    }

                    else{
                        alert(arrayListData[0][p]['company']+"is not configured of type"+arrayListData[0][p]['mode']);
                    }
                })

        }

    }

    saveLesserTransaction(){

        let obj={}
        obj['ratePerHr']=this.lesserConfiguration['ratePerHr']
        obj['ratePerKm']=this.lesserConfiguration['ratePerKm']
        obj['tsno']=this.formConfigData['TSNo']
        obj['totalDaysPerHour']=this.formConfigData['totalDaysPerHour']
        obj['totalKms']=this.formConfigData['totalKms']
        /* obj['bookedBy']=this.formConfigData['bookedBy']*/
        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
        obj['transactionEndDate']=this.bulkUpload.transactionEndDate
        obj['vehicleType']=this.formConfigData['vehicleType']
        obj['lesserName']=this.vechicleSelectedDetails['vehicleType']
        obj['vehicleNumber']=this.vechicleSelectedDetails['vehicleNumber']
        obj['corporateContract']=this.corporateContractType
        obj['companyTransactionSelected']=this.companyTransactionSelected['companySelectedForDropDown']
        if(this.formConfigData['totalKms']>=80){
            obj['extraKms']=this.formConfigData['totalKms']-80
        }
        else{
            obj['extraKms']=0
        }


        if(this.formConfigData['totalKms']>=1 && this.formConfigData['totalKms']<=6 && this.formConfigData['totalKms']>40){
            obj['extraKms']=this.formConfigData['totalKms']-40
        }

        else if(this.formConfigData['totalDaysPerHour']>6&& this.formConfigData['totalKms']>80){
            obj['extraKms']=this.formConfigData['totalKms']-80
        }
        else{
            obj['extraHrs']=0
        }

        if(this.formConfigData['totalDaysPerHour']>=1 && this.formConfigData['totalDaysPerHour']<=6){
            obj['slabRate']=this.formConfigData['slabRate4hrs40kmts']
        }
        else if(this.formConfigData['totalDaysPerHour']>=6){
            obj['slabRate']=this.formConfigData['slabRate8hrs80kmts']
        }


        if(this.formConfigData['totalDaysPerHour']>=8){
            obj['extraHrs']=this.formConfigData['totalDaysPerHour']-8
        }
        else{
            obj['extraHrs']=0
        }
        obj['extraKmAmt']=this.formConfigData['ratePerKm']*obj['extraKms']
        obj['extraHrAmt']=this.formConfigData['ratePerHr']*obj['extraHrs']
        obj['tripAmt']=obj['extraKmAmt']+ obj['extraHrAmt']+ obj['slabRate'];


        this.lesserTransactionService.saveLesserTransaction(obj)
            .subscribe((data: any) => {
               console.log("*******lesserTransactionService***********added*********");

            });
    }

    submitTransaction(){
        if(this.lesserConfiguration){
            this.saveLesserTransaction()
        }

        let obj={}
        obj['ratePerHr']=this.formConfigData['ratePerHr']
        obj['ratePerKm']=this.formConfigData['ratePerKm']
        obj['tsno']=this.formConfigData['TSNo']
        obj['totalDaysPerHour']=this.formConfigData['totalDaysPerHour']
        obj['totalKms']=this.formConfigData['totalKms']
       /* obj['bookedBy']=this.formConfigData['bookedBy']*/
        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
        obj['transactionEndDate']=this.bulkUpload.transactionEndDate
        obj['vehicleType']=this.formConfigData['vehicleType']
        obj['vehicleNumber']=this.vechicleSelectedDetails['vehicleNumber']
        obj['corporateContract']=this.corporateContractType
        obj['companyTransactionSelected']=this.companyTransactionSelected['companySelectedForDropDown']
        if(this.formConfigData['totalKms']>=1 && this.formConfigData['totalKms']<=6 && this.formConfigData['totalKms']>40){
            obj['extraKms']=this.formConfigData['totalKms']-40
        }

        else if(this.formConfigData['totalDaysPerHour']>6&& this.formConfigData['totalKms']>80){
            obj['extraKms']=this.formConfigData['totalKms']-80
        }



        if(this.formConfigData['totalDaysPerHour']>=1 && this.formConfigData['totalDaysPerHour']<=6){
            obj['slabRate']=this.formConfigData['slabRate4hrs40kmts']
        }
        else if(this.formConfigData['totalDaysPerHour']>=6){
            obj['slabRate']=this.formConfigData['slabRate8hrs80kmts']
        }

        if(this.formConfigData['totalDaysPerHour']>=8){
            obj['extraHrs']=this.formConfigData['totalDaysPerHour']-8
        }
        else{
            obj['extraHrs']=0
        }
        obj['extraKmAmt']=this.formConfigData['ratePerKm']*obj['extraKms']
        obj['extraHrAmt']=this.formConfigData['ratePerHr']*obj['extraHrs']
        obj['tripAmt']=obj['extraKmAmt']+ obj['extraHrAmt']+ obj['slabRate'];


        this.cctransactionService.saveCCTransaction(obj)
            .subscribe((data: any) => {
                this.getCCTransaction(0,10)
            });
    }

    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.formConfigData=[];
        this.companyTransactionSelected=""
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList;

            })
    }

    onvehicleSelect(vehicleDetails){
        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {
                console.log("******vechicleList*************");
                console.log(vehicleDetails);
                this.vechicleSelectedDetails=vehicleDetails
                this.vechicleSelectedVehicleType=vehicleDetails.vehicleType
                this.getLesserAssociationConfiguration(this.vechicleSelectedDetails)
            })
    }

    getLesserAssociationConfiguration(vehicleDetails){
        this.lesserAssociationService.getLesserAssociationByLesserNameAndCCType(vehicleDetails.lesserName,this.corporateContractType,vehicleDetails.vehicleType)
            .subscribe((lesserConfig: any) => {
                if(lesserConfig.length!=0){
                    this.lesserConfiguration=lesserConfig[0]
                }
                else{
                    alert("lesser is not avalibale for vehicle"+vehicleDetails.vehicleNumber)
                }


            })
    }


    onCompanySelect(companyDetails){
        this.companyTransactionSelected=companyDetails
        this.vehicleCorporateContractService.getVehicleCorporateContractByMongodbId(companyDetails['_id'])
            .subscribe((companyCC: any) => {
                this.vehicleCorporateContractService.getcompanyCCByMongodbIdAndCCTypeAndvehicleType( companyCC[0].companySelected._id ,this.corporateContractType
                    ,this.vechicleSelectedVehicleType)
                    .subscribe((companyCCDetails: any) => {
                        if(companyCCDetails && companyCCDetails[0]){
                            companyCCDetails=companyCCDetails[0];
                            for(var g=0;g<this.corporateContractFieldsService.corporateContractTypes.length;g++){
                                if(this.corporateContractFieldsService.corporateContractTypes[g]['assetType']['typeId']==
                                    companyCCDetails.corporateContract){
                                    let CCFieldsConfiguration = Object.keys(this.corporateContractFieldsService.corporateContractTypes[g].configuration);
                                    let companyCCKeys = Object.keys(companyCCDetails);
                                    let VehicleCCKeys = Object.keys(this.vechicleSelectedDetails);
                                    if(companyCCKeys){
                                        for(var k=0;k<companyCCKeys.length;k++){
                                            for(let p=0;p<CCFieldsConfiguration.length;p++){
                                                if(companyCCKeys[k]== CCFieldsConfiguration[p]){
                                                    this.formConfigData
                                                        [this.corporateContractFieldsService.corporateContractTypes[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                        companyCCDetails[companyCCKeys[k]]
                                                }
                                            }
                                        }

                                        this.formConfigData['vehicleType']=this.vechicleSelectedVehicleType;

                                    }
                                    console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                    this.fromConfigObjectForFrom(this.corporateContractFieldsService.corporateContractTypes[g],companyCCDetails)
                                }

                            }
                        }
                        else {
                            alert("selected vehicle type  is not configured")
                        }
                    });
            })



    }

    fromConfigObjectForFrom(configDetailsObj,companyCCDetails){
        let configObj=  configDetailsObj.configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for ( var prop of responseKeys) {
            formData.push(configObj[prop]);
        }
        if(formData){
            this.corporateContractFields=formData;
        }


    }

    deleteCC1Trasnaction(ccDetails){
        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction(0,10)
            })
    }

    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }

    getCCTransaction(skip,limit) {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CC3List =CCListDetails
                this.setPage(1);
            });
    }

    getAllCompanyOfTypeCC3(){
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                console.log(companyList)
                this.companyAssetDropDown =_.uniqBy(companyList,function(companyList){ return companyList['companySelectedForDropDown']});
            })
    }

    constructor(public cctransactionService: CctransactionService,
                private assetService:AssetService,
                private vechicleTypeServiceService: VechicleTypeServiceService,
                private vehicleCorporateContractService:VehicleCorporateContractService,
                private corporateContractFieldsService:CorporateContractFieldsService,
                public corporateContractTypeService:CorporateContractTypeService,
                public excelService:ExcelService, private pagerService: PagerService,
                private lesserTransactionService:LesserTransactionService,
                private lesserAssociationService:LesserAssociationService) {

    }

    ngOnInit() {
        this.getCCTransaction(0,10)
    }

    data: any = [{
        SlNo: 0,
        date: "",
        vehicleNumber: "",
        tsno:"",
        mode:0,
        company:"",
        totalKms:0,
        totalDaysPerHour:0
    }];

    exportAsXLSX():void {
        this.excelService.exportAsExcelFile(this.data, 'onDemand');
    }
    private allItems: any[];

    // pager object
    pager: any = {};

    // paged items
    pagedItems: any[];


    setPage(page: number) {
        // get pager object from service
        this.pager = this.pagerService.getPager(this.CC3List.length, page);

        // get current page of items
        this.resultList = this.CC3List.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }


}
