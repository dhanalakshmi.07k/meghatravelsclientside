import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnDemandCorporateContractComponent } from './on-demand-corporate-contract.component';

describe('OnDemandCorporateContractComponent', () => {
  let component: OnDemandCorporateContractComponent;
  let fixture: ComponentFixture<OnDemandCorporateContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnDemandCorporateContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnDemandCorporateContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
