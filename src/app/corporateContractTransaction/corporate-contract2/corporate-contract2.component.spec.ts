import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateContract2Component } from './corporate-contract2.component';

describe('CorporateContract2Component', () => {
  let component: CorporateContract2Component;
  let fixture: ComponentFixture<CorporateContract2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateContract2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateContract2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
