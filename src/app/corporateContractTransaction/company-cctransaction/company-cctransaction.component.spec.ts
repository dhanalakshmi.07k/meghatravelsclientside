import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCctransactionComponent } from './company-cctransaction.component';

describe('CompanyCctransactionComponent', () => {
  let component: CompanyCctransactionComponent;
  let fixture: ComponentFixture<CompanyCctransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyCctransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCctransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
