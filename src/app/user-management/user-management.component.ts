import { Component, OnInit } from '@angular/core';
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {FormService} from "../../services/form.service";
import {UserManagementService} from "../../services/user-management.service";

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
    assetFromConfig: any;
    assetConfigParameterType: any;
    mainAsset: any;
    assetId: any;
    assetConfigName: any;
    public skip: number;
    public limit: number;
    public allAssets: any = [];
    public pagination: any;
  constructor(public applicationParameteConfigService: ApplicationParameteConfigService, public formService: FormService,
              public userManagementService:UserManagementService) {
      this.pagination = {
          'currentPage': 1,
          'totalPageCount': 0,
          'recordsPerPage': 12
      };
  }

  ngOnInit() {
      this.getAssetTotalCountForPagination()
      this.getUsersBasedOnrange(0, 10)
  }

    getAssetTotalCountForPagination() {
        this.userManagementService.getUserCount()
            .subscribe((userCount: any) => {
                this.pagination.totalPageCount = userCount.count;
            });
    }

    getUsersBasedOnrange(skip, limit) {
        this.userManagementService.getAllUser(skip, limit)
            .subscribe(allAssets => {
                this.allAssets = allAssets;
            });
    }

    deleteShiftTypeCostConfiguration(assetFuelDetails) {
        this.userManagementService.deleteUserByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getUsersBasedOnrange(0, 10)
            });
    }

    getEditConfiguration(assetDetails) {
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("userConfig")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "userConfig"
                // console.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }

    savedAssetCardToAssetList(savedAssetValue) {

        this.getUsersBasedOnrange(0, 10)
    }

    updateAssetCardToAssetList(editAssetValue) {

        this.getUsersBasedOnrange(0, 10)
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("userConfig")
            .subscribe((data: any) => {
                this.mainAsset = false;
                this.assetConfigName = "userConfig";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }

    notifyUser(MongodbID){
      console.log(MongodbID._id)
        this.userManagementService.notifyUserEmail(MongodbID._id)
            .subscribe((data: any) => {
                    console.log("daydouahsdohasd")
                    console.log(data)
                },
                err => {
                    // this.emailStatus = err.error.text;

                });
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getUsersBasedOnrange(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getUsersBasedOnrange(this.skip, this.limit);
        }
    }



}
