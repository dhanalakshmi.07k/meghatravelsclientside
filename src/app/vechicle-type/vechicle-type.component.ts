import { Component, OnInit } from '@angular/core';
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {FormService} from "../../services/form.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";

@Component({
  selector: 'app-vechicle-type',
  templateUrl: './vechicle-type.component.html',
  styleUrls: ['./vechicle-type.component.scss']
})
export class VechicleTypeComponent implements OnInit {

    assetFromConfig: any;
    assetConfigParameterType: any;
    mainAsset: any;
    assetId: any;
    assetConfigName: any;
    public skip: number;
    public limit: number;
    public allAssets: any = [];
    public pagination: any;


    constructor(public applicationParameteConfigService: ApplicationParameteConfigService, public formService: FormService,
                public vechicleTypeServiceService:VechicleTypeServiceService) {

        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
    }

  ngOnInit() {
      this.getAssetTotalCountForPagination()
      this.getVechicleTypeCostBasedOnrange(0, 10)

  }

    getAssetTotalCountForPagination() {
        this.vechicleTypeServiceService.getVechicleTypeCostCount()
            .subscribe((fuelCount: any) => {
                this.pagination.totalPageCount = fuelCount.count;
            });
    }
    getVechicleTypeCostBasedOnrange(skip, limit) {
        this.vechicleTypeServiceService.getAllVechicleType(skip, limit)
            .subscribe(allAssets => {

                this.allAssets = allAssets;
                /*     this.isShowAsset=false
                     this.allAssets = allAssets;*/
            });
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("VehicleType")
            .subscribe((data: any) => {
                this.mainAsset = false;
                this.assetConfigName = "VehicleType";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }

    getEditConfiguration(assetDetails) {
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("VehicleType")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "VehicleType"
                // console.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }


    savedAssetCardToAssetList(savedAssetValue) {

        this.getVechicleTypeCostBasedOnrange(0, 10)
    }

    updateAssetCardToAssetList(editAssetValue) {

        this.getVechicleTypeCostBasedOnrange(0, 10)
    }




    deleteShiftTypeCostConfiguration(assetFuelDetails) {

        this.vechicleTypeServiceService.deleteVechicleTypeByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getVechicleTypeCostBasedOnrange(0, 10)
            });
    }
deleteTransactionData:any
intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }


}
