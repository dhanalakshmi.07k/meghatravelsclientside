import {Component, Input,EventEmitter,Output, OnInit} from '@angular/core';
import {VechicleTransactionService} from "../../services/vechicle-transaction.service";
import {FormService} from "../../services/form.service";

@Component({
  selector: 'app-vechicle-transaction-edit-from',
  templateUrl: './vechicle-transaction-edit-from.component.html',
  styleUrls: ['./vechicle-transaction-edit-from.component.scss']
})
export class VechicleTransactionEditFromComponent implements OnInit {
    @Output() updateAssetConfig: EventEmitter<string> = new EventEmitter<string>()
    @Input() formConfigData: any;
    @Input() assetParameterConfigName: any;
    @Input() assetId: string;
    @Input() mainStatusAsset: any;

  constructor(public vechicleTransactionService:VechicleTransactionService,
              public formService:FormService) { }



    submitEditConfigDetails() {
        this.vechicleTransactionService.updateVechicleTransactionById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
            .subscribe((assetvalues: any) => {
                console.log("ouput:::::: console.log(\"ouput:::::assetIdassetId:::::\")::::")
                console.log(assetvalues)
                // $('#sidebar-right-to-add').modal('hide');
                this.updateAssetConfig.emit(assetvalues);
            })

    }


    ngOnInit() {
  }

}
