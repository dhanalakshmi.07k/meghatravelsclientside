import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VechicleTransactionEditFromComponent } from './vechicle-transaction-edit-from.component';

describe('VechicleTransactionEditFromComponent', () => {
  let component: VechicleTransactionEditFromComponent;
  let fixture: ComponentFixture<VechicleTransactionEditFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VechicleTransactionEditFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VechicleTransactionEditFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
