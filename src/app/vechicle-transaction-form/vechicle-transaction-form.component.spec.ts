import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VechicleTransactionFormComponent } from './vechicle-transaction-form.component';

describe('VechicleTransactionFormComponent', () => {
  let component: VechicleTransactionFormComponent;
  let fixture: ComponentFixture<VechicleTransactionFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VechicleTransactionFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VechicleTransactionFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
