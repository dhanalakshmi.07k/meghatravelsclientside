import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormService} from "../../services/form.service";
import {AssetService} from "../../services/asset.service";
import {FuelServiceService} from "../../services/fuel-service.service";
import {ServiceTypeServiceService} from "../../services/service-type-service.service";
import {LessorTypeServiceService} from "../../services/lessor-type-service.service";
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {ExpenseTypeServiceService} from "../../services/expense-type-service.service";
import {CorporateContractTypeService} from "../../services/corporate-contract-type.service";
import {VechicleHistoryService} from "../../services/vechicle-history.service";
import {UserManagementService} from "../../services/user-management.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";

declare var $: any;


@Component({
    selector: 'app-asset-edit-from',
    templateUrl: './asset-edit-from.component.html',
    styleUrls: ['./asset-edit-from.component.scss']
})
export class AssetEditFromComponent implements OnInit {

    Object = Object;
    @Input() formConfigData: any;
    @Input() assetParameterConfigName: any;
    @Input() assetId: string;
    @Input() mainStatusAsset: any;
    @Input() assetConfigParameterType: any;
    @Output() event: EventEmitter<string> = new EventEmitter<string>()
    @Output() updateAssetCart: EventEmitter<string> = new EventEmitter<string>()
    @Output() updateAssetConfig: EventEmitter<string> = new EventEmitter<string>()


    constructor(public formService: FormService, public assetService: AssetService, public fuelServiceService: FuelServiceService
        , public serviceTypeServiceService: ServiceTypeServiceService, public lessorTypeServiceService: LessorTypeServiceService,
                public shiftTypeServiceService: ShiftTypeServiceService, public expenseTypeServiceService: ExpenseTypeServiceService
        , public corporateContractTypeService: CorporateContractTypeService,public vechicleHistoryService: VechicleHistoryService,
                public userManagementService:UserManagementService,
                public vechicleTypeServiceService:VechicleTypeServiceService) {
    }

    submitEditConfigDetails() {
        this.assetService.updateAssetsById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
            .subscribe((assetvalues: any) => {
                console.log("ouput:::::: console.log(\"ouput:::::assetIdassetId:::::\")::::")
                console.log(assetvalues)
                // $('#sidebar-right-to-add').modal('hide');
                this.updateAssetCart.emit(assetvalues);
            })

    }


    submitParameterConfigDetails() {


        if (this.assetConfigParameterType === "FuelType") {
            this.fuelServiceService.updateFuelTypeById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }

        if (this.assetConfigParameterType === "ServiceType") {
            this.serviceTypeServiceService.updateServiceTypeById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }


        if (this.assetConfigParameterType === "LessorType") {
            this.lessorTypeServiceService.updateLesserTypeById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }

        if (this.assetConfigParameterType == "ShiftTypeCost") {
            this.shiftTypeServiceService.updateShiftTypeCostById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }

        if (this.assetConfigParameterType == "ExpenseType") {
            this.expenseTypeServiceService.updateExpenseTypeById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }
        if (this.assetConfigParameterType === "corporateContractType") {
            this.corporateContractTypeService.updateCorporateContractTypeById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }

        if (this.assetConfigParameterType === "vechicleHistoryType") {
            this.vechicleHistoryService.updateVechicleHistoryById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }

        if (this.assetConfigParameterType === "userConfig") {
            this.userManagementService.updateUserById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }

        if (this.assetConfigParameterType === "VehicleType") {
            this.vechicleTypeServiceService.updateVechicleTypeById(this.assetId, this.formService.formatEditAssetDetails(this.formConfigData))
                .subscribe((assetvalues: any) => {
                    $('#edit').modal('hide');
                    this.updateAssetConfig.emit(assetvalues);

                })
        }


    }


    ngOnInit() {
    }

}
