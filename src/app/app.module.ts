import {BrowserModule} from '@angular/platform-browser';
import {NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {CountCardComponent} from './count-card/count-card.component';
import {AssetListComponent} from './asset-list/asset-list.component';
import {SideMenuComponent} from './core/side-menu/side-menu.component';
import {HeaderComponent} from './core/header/header.component';
import {AssetService} from '../services/asset.service';
import {LoginComponent} from './login/login.component';
import {LoginHeaderComponent} from './core/login-header/login-header.component';
import {AssetFormComponent} from './asset-form/asset-form.component';
import {FormService} from '../services/form.service';
import {AssetCardComponent} from './asset-card/asset-card.component';
import {RightSidebarComponent} from './core/right-sidebar/right-sidebar.component';
import {AddAssetListComponent} from './core/add-asset-list/add-asset-list.component';
import {AssetEditFromComponent} from './asset-edit-from/asset-edit-from.component';
import {Ng2SearchPipeModule} from 'ng2-search-filter'; //importing the module
import {Ng2OrderModule} from 'ng2-order-pipe'; //importing the module
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import {AlertPopupComponent} from './core/alert-popup/alert-popup.component';
import {RightSiderToAddComponent} from './core/right-sider-to-add/right-sider-to-add.component';
import {PaginationComponent} from './pagination/pagination.component';
import {PagerService} from "../services/pager.service";
import {AssetCardNearableComponent} from './asset-card-nearable/asset-card-nearable.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AssetLinkedCountComponent} from './core/asset-linked-count/asset-linked-count.component';
import {SettingsComponent} from "./settings/settings.component";
import {ReportComponent} from './report/report/report.component';
import {ConfigService} from "../services/config.service";
import {LoaderComponent} from './loader/loader.component';
import {HTTPListener, HTTPStatus} from '../interceptors/HttpInterceptor';
import {LessorTypeComponent} from './lessor-type/lessor-type.component';
import {ShiftTypeCostComponent} from './shift-type-cost/shift-type-cost.component';
import {ExpenseTypeComponent} from './expense-type/expense-type.component';
import {FuelTypeComponent} from './fuel-type/fuel-type.component';
import {ServiceTypeComponent} from './service-type/service-type.component';
import {Ng2SmartTableModule} from "ng2-smart-table";
import {FuelServiceService} from "../services/fuel-service.service";
import {ApplicationParameteConfigService} from "../services/application-paramete-config.service";
import {ServiceTypeServiceService} from "../services/service-type-service.service";
import {LessorTypeServiceService} from "../services/lessor-type-service.service";
import {ShiftTypeServiceService} from "../services/shift-type-service.service";
import {ExpenseTypeServiceService} from "../services/expense-type-service.service";
import {CorporateContractTypeService} from "../services/corporate-contract-type.service";
import {VechicleCardComponent} from './asset-card/vechicle-card/vechicle-card.component';
import {CompanyCardComponent} from './asset-card/company-card/company-card.component';
import {LesserCardComponent} from './asset-card/lesser-card/lesser-card.component';
import {DriverCardComponent} from './asset-card/driver-card/driver-card.component';
import { VechicleHistoryComponent } from './vechicle-history/vechicle-history.component';
import {VechicleHistoryService} from "../services/vechicle-history.service";
import { UserManagementComponent } from './user-management/user-management.component';
import {UserManagementService} from "../services/user-management.service";
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { VehicleExpenseComponent } from './vehicle-expense/vehicle-expense.component';
import {VechicleExpenseServiceService} from "../services/vechicle-expense-service.service";
import {CorporateContractFieldsService} from "../services/corporate-contract-fields.service";
import { VechicleTransactionComponent } from './vechicle-transaction/vechicle-transaction.component';
import {VechicleTransactionService} from "../services/vechicle-transaction.service";
import { VechicleTransactionFormComponent } from './vechicle-transaction-form/vechicle-transaction-form.component';
import { VechicleTransactionEditFromComponent } from './vechicle-transaction-edit-from/vechicle-transaction-edit-from.component';
import { VechicleTypeComponent } from './vechicle-type/vechicle-type.component';
import {VechicleTypeServiceService} from "../services/vechicle-type-service.service";
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { VehicleCorporateContractComponent } from './companyAssociation/vehicle-corporate-contract/vehicle-corporate-contract.component';
import {VehicleCorporateContractService} from '../services/vehicle-corporate-contract.service';
import { CompanySliderComponent } from './core/company-slider/company-slider.component';
import { DriverSliderComponent } from './core/driver-slider/driver-slider.component';
import { LesserSliderComponent } from './core/lesser-slider/lesser-slider.component';
import { VehicleSliderComponent } from './core/vehicle-slider/vehicle-slider.component';
import { DriverAssetFromComponent } from './dashboard/assetForm/driver-asset-from/driver-asset-from.component';
import { VechicleAssetFromComponent } from './dashboard/assetForm/vechicle-asset-from/vechicle-asset-from.component';
import { LesserAssetFromComponent } from './dashboard/assetForm/lesser-asset-from/lesser-asset-from.component';
import { CompanyAssetFromComponent } from './dashboard/assetForm/company-asset-from/company-asset-from.component';
import { CorporateContract1Component } from './corporate-contract1/corporate-contract1.component';
import { CorporateContract2Component } from './corporateContractTransaction/corporate-contract2/corporate-contract2.component';
import { CorporateContract3Component } from './corporate-contract3/corporate-contract3.component';
import {CctransactionService} from '../services/cctransaction.service';
import {ComponetCommunicationService} from '../services/componet-communication.service';
import { CompanyCctransactionComponent } from './corporateContractTransaction/company-cctransaction/company-cctransaction.component';
import {AssetCommunicationService} from "../services/asset-communication.service";
import { CorporateContract4Component } from './corporate-contract4/corporate-contract4.component';
import { CorporateContract5Component } from './corporate-contract5/corporate-contract5.component';
import { CorporateContract6Component } from './corporate-contract6/corporate-contract6.component';
import { CorporateContract7Component } from './corporate-contract7/corporate-contract7.component';
import {FilterPipe} from './core/filter/pipes';
import { OnDemandCorporateContractComponent } from './corporateContractTransaction/on-demand-corporate-contract/on-demand-corporate-contract.component';
import {OnDemandCompanyContractComponent} from "./on-demand-company-contract/on-demand-company-contract.component";
import { OnDemandshiftTypeComponent } from './corporateContractTransaction/on-demandshift-type/on-demandshift-type.component';
import { VehicleCorporteAssociation1Component } from './companyAssociation/vehicle-corporte-association1/vehicle-corporte-association1.component';
import { VehicleSiftCorporteAssociation2Component } from './companyAssociation/vehicle-sift-corporte-association2/vehicle-sift-corporte-association2.component';
import { CorporatContractFixedDayKmContractComponent } from './corporat-contract-fixed-day-km-contract/corporat-contract-fixed-day-km-contract.component';
import { OnDemandAssociationComponent } from './companyAssociation/on-demand-association/on-demand-association.component';
import { CompanyCorporateContract2Component } from './companyAssociation/company-corporate-contract2/company-corporate-contract2.component';
import { VehicleCoporateAssociationHeaderComponent } from './vehicle-coporate-association-header/vehicle-coporate-association-header.component';
import {ExcelService} from '../services/excel.service';
import {GrdFilterPipe} from "../services/search-filter";
import { DriverAssetComponent } from './dashboard/driver-asset/driver-asset.component';
import { CompanyAssetComponent } from './dashboard/company-asset/company-asset.component';
import { VehicleAssetComponent } from './dashboard/vehicle-asset/vehicle-asset.component';
import { LesserAssetComponent } from './dashboard/lesser-asset/lesser-asset.component';
import { AssetButtonComponent } from './dashboard/asset-button/asset-button.component';
import { OnDemandUsageReportComponent } from './report/on-demand-usage-report/on-demand-usage-report.component';
import { CompanyWiseSummaryComponent } from './report/company-wise-summary/company-wise-summary.component';
import { ReportButtonComponent } from './report/report-button/report-button.component';
import {ReportServiceService} from "../services/reportService/report-service.service";
import { LesserOndemandAssociationComponent } from './lesserAssociation/lesser-ondemand-association/lesser-ondemand-association.component';
import { LsserShiftTypeAssoicationComponent } from './lesserAssociation/lsser-shift-type-assoication/lsser-shift-type-assoication.component';
import { LesserCommonTypeAssociationComponent } from './lesserAssociation/lesser-common-type-association/lesser-common-type-association.component';
import { Lessercc2TypeAssocationComponent } from './lesserAssociation/lessercc2-type-assocation/lessercc2-type-assocation.component';
import { LesserHeaderComponent } from './lesserAssociation/lesser-header/lesser-header.component';
import {LesserAssociationService} from '../services/lesserAssociation/lesser-association.service';
import {LesserTransactionService} from "../services/lesserAssociation/lesser-transaction.service";
import { LesserReportComponent } from './report/lesser-report/lesser-report.component';
import { CompanyLesserHeaderComponent } from './companyLesserAssociationHeader/company-lesser-header/company-lesser-header.component';


const RxJS_Services = [HTTPListener, HTTPStatus];


@NgModule({
    declarations: [
        AppComponent,
        CountCardComponent,
        AssetListComponent,
        SideMenuComponent,
        HeaderComponent,
        LoginComponent,
        LoginHeaderComponent,
        AssetFormComponent,
        AssetCardComponent,
        RightSidebarComponent,
        AddAssetListComponent,
        AssetEditFromComponent,
        AlertPopupComponent,
        RightSiderToAddComponent,
        PaginationComponent,
        AssetLinkedCountComponent,
        AssetCardNearableComponent,
        SettingsComponent,
        ReportComponent,
        LoaderComponent,
        LessorTypeComponent,
        ShiftTypeCostComponent,
        ExpenseTypeComponent,
        FuelTypeComponent,
        ServiceTypeComponent,
        VechicleCardComponent,
        CompanyCardComponent,
        LesserCardComponent,
        DriverCardComponent,
        VechicleHistoryComponent,
        UserManagementComponent,
        ForgotpasswordComponent,
        VehicleExpenseComponent,
        VechicleTransactionComponent,
        VechicleTransactionFormComponent,
        VechicleTransactionEditFromComponent,
        VechicleTypeComponent,
        VehicleCorporateContractComponent,
        CompanySliderComponent,
        DriverSliderComponent,
        LesserSliderComponent,
        VehicleSliderComponent,
        DriverAssetFromComponent,
        VechicleAssetFromComponent,
        LesserAssetFromComponent,
        CompanyAssetFromComponent,
        CorporateContract1Component,
        CorporateContract2Component,
        CorporateContract3Component,
        CompanyCctransactionComponent,
        CorporateContract4Component,
        CorporateContract5Component,
        CorporateContract6Component,
        CorporateContract7Component,
        OnDemandCompanyContractComponent,
        FilterPipe,
        OnDemandCorporateContractComponent,
        OnDemandshiftTypeComponent,
        VehicleCorporteAssociation1Component,
        VehicleSiftCorporteAssociation2Component,
        CorporatContractFixedDayKmContractComponent,
        OnDemandAssociationComponent,
        CompanyCorporateContract2Component,
        VehicleCoporateAssociationHeaderComponent,
        GrdFilterPipe,
        DriverAssetComponent,
        CompanyAssetComponent,
        VehicleAssetComponent,
        LesserAssetComponent,
        AssetButtonComponent,
        OnDemandUsageReportComponent,
        CompanyWiseSummaryComponent,
        ReportButtonComponent,
        LesserOndemandAssociationComponent,
        LsserShiftTypeAssoicationComponent,
        LesserCommonTypeAssociationComponent,
        Lessercc2TypeAssocationComponent,
        LesserHeaderComponent,
        LesserReportComponent,
        CompanyLesserHeaderComponent

    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule,
        NgMultiSelectDropDownModule.forRoot(),
        MDBBootstrapModule.forRoot(),
        Ng2SearchPipeModule, //including into imports
        Ng2OrderModule, // importing the sorting package here
        NgxPaginationModule,
        Ng2SmartTableModule,
        NgbModule.forRoot(),

    ],
    schemas: [NO_ERRORS_SCHEMA],
    providers: [
        AssetService,
        FormService,
        PagerService,
        ConfigService,
        FuelServiceService,
        ApplicationParameteConfigService,
        ServiceTypeServiceService,
        LessorTypeServiceService,
        ShiftTypeServiceService,
        ExpenseTypeServiceService,
        CorporateContractTypeService,
        VechicleHistoryService,
        UserManagementService,
        VechicleExpenseServiceService,
        CorporateContractFieldsService,
        VechicleTransactionService,
        VechicleTypeServiceService,
        VehicleCorporateContractService,
        CctransactionService,
        ComponetCommunicationService,
        AssetCommunicationService,
        ExcelService,
        ReportServiceService,
        LesserAssociationService,
        LesserTransactionService,
        RxJS_Services,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HTTPListener,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
