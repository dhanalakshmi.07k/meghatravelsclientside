import {Component, OnInit} from '@angular/core';
import {FormService} from "../../services/form.service";
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {ExpenseTypeServiceService} from "../../services/expense-type-service.service";

@Component({
    selector: 'app-expense-type',
    templateUrl: './expense-type.component.html',
    styleUrls: ['./expense-type.component.scss']
})
export class ExpenseTypeComponent implements OnInit {

    assetFromConfig: any;
    assetConfigParameterType: any;
    mainAsset: any;
    assetId: any;
    assetConfigName: any;
    public skip: number;
    public limit: number;
    public allAssets: any = [];
    public pagination: any;

    constructor(public applicationParameteConfigService: ApplicationParameteConfigService, public formService: FormService,
                public expenseTypeServiceService: ExpenseTypeServiceService) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getExpenseTypeBasedOnrange(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getExpenseTypeBasedOnrange(this.skip, this.limit);
        }
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("ExpenseType")
            .subscribe((data: any) => {
                console.log("confog datae09000934-02943-0")
                console.log(data)
                this.mainAsset = false;
                this.assetConfigName = "ExpenseType";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }


    getExpenseTypeBasedOnrange(skip, limit) {
        this.expenseTypeServiceService.getAllExpenseType(skip, limit)
            .subscribe(allAssets => {
                this.allAssets = allAssets;
            });
    }

    getExpenseTotalCountForPagination() {
        this.expenseTypeServiceService.getExpenseTypeCostCount()
            .subscribe((expenseCount: any) => {
                this.pagination.totalPageCount = expenseCount.count;
            });
    }

    getEditConfiguration(assetDetails) {
        console.log("confog datae09000934-02943-0")
        console.log(assetDetails._id)
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("ExpenseType")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "ExpenseType"
                // console.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }


    deleteExpenseTypeConfiguration(assetFuelDetails) {
        this.expenseTypeServiceService.deleteExpenseTypeByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getExpenseTypeBasedOnrange(0, 10)
            });
    }

    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }





    savedAssetCardToAssetList(savedAssetValue) {
        this.getExpenseTypeBasedOnrange(0, 10)
    }

    updateAssetCardToAssetList(editAssetValue) {
        this.getExpenseTypeBasedOnrange(0, 10)
    }

    ngOnInit() {
        this.getExpenseTypeBasedOnrange(0, 10)
        this.getExpenseTotalCountForPagination()
    }

}
