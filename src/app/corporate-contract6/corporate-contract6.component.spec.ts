import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateContract6Component } from './corporate-contract6.component';

describe('CorporateContract6Component', () => {
  let component: CorporateContract6Component;
  let fixture: ComponentFixture<CorporateContract6Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateContract6Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateContract6Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
