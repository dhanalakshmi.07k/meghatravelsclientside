/*https://github.com/akveo/ng2-smart-table*/
import {Component, OnInit} from '@angular/core';
import {LessorTypeServiceService} from "../../services/lessor-type-service.service";
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {FormService} from "../../services/form.service";

@Component({
    selector: 'app-lessor-type',
    templateUrl: './lessor-type.component.html',
    styleUrls: ['./lessor-type.component.scss']
})
export class LessorTypeComponent implements OnInit {
    public mainAsset: any;
    public assetConfigParameterType: any;
    public assetId: any;
    public assetFromConfig: any;
    public assetConfigName: any;
    public allLessorType: any = [];
    public pagination: any;
    public skip: number;
    public limit: number
    private allItems: any[];

    constructor(public lessorTypeServiceService: LessorTypeServiceService,
                public applicationParameteConfigService: ApplicationParameteConfigService,
                public formService: FormService) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getLesserTypeBasedOnrange(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getLesserTypeBasedOnrange(this.skip, this.limit);
        }
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("LessorType")
            .subscribe((data: any) => {
                console.log("confog datae09000934-02943-0")
                console.log(data)
                this.mainAsset = false;
                this.assetConfigName = "LessorType";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }

    savedAssetCardToAssetList(savedAssetValue) {
        this.getLesserTypeBasedOnrange(0, 10)
    }

    ngOnInit() {
        this.getLesserTypeBasedOnrange(0, 10)
    }

    updateAssetCardToAssetList(editAssetValue) {
        this.getLesserTypeBasedOnrange(0, 10)
    }


    getExpenseTotalCountForPagination() {
        this.lessorTypeServiceService.getLesserTypeCount()
            .subscribe((lesserCount: any) => {
                this.pagination.totalPageCount = lesserCount.count;
            });
    }


    getEditConfiguration(assetDetails) {
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("LessorType")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "LessorType"
                // console.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }

    deleteFuelTypeConfiguration(assetFuelDetails) {
        console.log("deleteLesserTypeByMongodbId ")
        console.log(assetFuelDetails._id)
        this.lessorTypeServiceService.deleteLesserTypeByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getLesserTypeBasedOnrange(0, 10)
            });
    }


    getLesserTypeBasedOnrange(skip, limit) {
        this.lessorTypeServiceService.getAllLesserType(skip, limit)
            .subscribe(allAssets => {
                console.log(allAssets)
                this.allLessorType = allAssets;
                /*     this.isShowAsset=false
                     this.allAssets = allAssets;*/
            });
    }

}
