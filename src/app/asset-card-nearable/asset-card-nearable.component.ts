import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-asset-card-nearable',
    templateUrl: './asset-card-nearable.component.html',
    styleUrls: ['./asset-card-nearable.component.scss']
})
export class AssetCardNearableComponent implements OnInit {
    @Input() assetData: any;
    public icon: string;
    public embossedFlag: boolean = false;
    @Output() registerAssetDetails: EventEmitter<any> = new EventEmitter<any>()

    constructor() {
    }

    ngOnInit() {
    }

    registerAsset() {
        if (this.assetData && !this.assetData.isRegistered) {
            this.embossedFlag = !this.embossedFlag;
            let obj = {};
            obj['embossedFlag'] = this.embossedFlag;
            obj['assetData'] = this.assetData.assetData;
            this.registerAssetDetails.emit(obj);
        }
    }

}
