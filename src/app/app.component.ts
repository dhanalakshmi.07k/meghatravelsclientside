import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {HTTPStatus} from '../interceptors/HttpInterceptor';

declare var bodymovin: any;

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'app';
    HTTPActivity: boolean;
    public showAsset

    constructor(private httpStatus: HTTPStatus, public router: Router) {

        this.httpStatus.getHttpStatus().subscribe((status: boolean) => {
            this.HTTPActivity = status;
            /* console.log("*********getHttpStatus*******app**********")
           console.log(status)*/
            this.showAsset = status

        });


    }
}
