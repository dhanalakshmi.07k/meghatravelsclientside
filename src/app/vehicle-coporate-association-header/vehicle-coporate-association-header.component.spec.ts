import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleCoporateAssociationHeaderComponent } from './vehicle-coporate-association-header.component';

describe('VehicleCoporateAssociationHeaderComponent', () => {
  let component: VehicleCoporateAssociationHeaderComponent;
  let fixture: ComponentFixture<VehicleCoporateAssociationHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleCoporateAssociationHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleCoporateAssociationHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
