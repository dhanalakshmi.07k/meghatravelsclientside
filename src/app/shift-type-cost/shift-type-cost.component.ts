import {Component, OnInit} from '@angular/core';
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {FormService} from "../../services/form.service";
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";

@Component({
    selector: 'app-shift-type-cost',
    templateUrl: './shift-type-cost.component.html',
    styleUrls: ['./shift-type-cost.component.scss']
})
export class ShiftTypeCostComponent implements OnInit {

    assetFromConfig: any;
    assetConfigParameterType: any;
    mainAsset: any;
    assetId: any;
    assetConfigName: any;
    public skip: number;
    public limit: number;
    public allAssets: any = [];
    public pagination: any;

    constructor(public applicationParameteConfigService: ApplicationParameteConfigService, public formService: FormService,
                public shiftTypeServiceService: ShiftTypeServiceService) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
    }

    ngOnInit() {
        this.getAssetTotalCountForPagination()
        this.getShiftTypeCostBasedOnrange(0, 10)
    }


    getAssetTotalCountForPagination() {
        this.shiftTypeServiceService.getShiftTypeCostCount()
            .subscribe((fuelCount: any) => {
                this.pagination.totalPageCount = fuelCount.count;
            });
    }
    getShiftTypeCostBasedOnrange(skip, limit) {
        this.shiftTypeServiceService.getAllShiftTypeCost(skip, limit)
            .subscribe(allAssets => {

                this.allAssets = allAssets;
                /*     this.isShowAsset=false
                     this.allAssets = allAssets;*/
            });
    }


    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getShiftTypeCostBasedOnrange(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getShiftTypeCostBasedOnrange(this.skip, this.limit);
        }
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("ShiftTypeCost")
            .subscribe((data: any) => {
                this.mainAsset = false;
                this.assetConfigName = "ShiftTypeCost";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }


    getEditConfiguration(assetDetails) {
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("ShiftTypeCost")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "ShiftTypeCost"
                // console.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }


    savedAssetCardToAssetList(savedAssetValue) {

        this.getShiftTypeCostBasedOnrange(0, 10)
    }

    updateAssetCardToAssetList(editAssetValue) {

        this.getShiftTypeCostBasedOnrange(0, 10)
    }





    deleteShiftTypeCostConfiguration(assetFuelDetails) {

        this.shiftTypeServiceService.deleteShiftTypeCostByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getShiftTypeCostBasedOnrange(0, 10)
            });
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }


}
