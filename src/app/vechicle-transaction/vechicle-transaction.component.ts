import { Component, OnInit } from '@angular/core';
import {VechicleTransactionService} from "../../services/vechicle-transaction.service";
import {FormService} from "../../services/form.service";
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {ShiftTypeServiceService} from "../../services/shift-type-service.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";
import {AssetService} from "../../services/asset.service";

@Component({
  selector: 'app-vechicle-transaction',
  templateUrl: './vechicle-transaction.component.html',
  styleUrls: ['./vechicle-transaction.component.scss']
})
export class VechicleTransactionComponent implements OnInit {


    public pagination: any;
    public mainAsset: any;
    public assetConfigName: any;
    public shiftTypesDropDown: any;
    public vechicleTypesDropDown: any;
    public assetFromConfig: any;
    public allTransactionList: any;
    public transactionDate: any;

    public editTransactionDetails={
        TSno:"",
        vechicleNo:"",
        vehicleMode:"",
        bookedBy:"",
        totalDay:"",
        totalHrs:"",
        totalKms:"",

    }



    public assetId: any;
    public selectedVechicleId: any;
    public selectedCompanyId: any;
    public selectedVehicleItems: any;
    public selectedCompanyItems: any;
    public vechicleDropdownList = [];
    public companyDropdownList = [];

    public vechicleTranctionFromDetails={
        TSno:"",
        vehicleMode:"",
        tripDate:new Date()
        }

    public vechicleTranctionAdditionalFromDetails={
        bookedBy:"",
        totalDay:0,
        totalHrs:0,
        totalKms:0,
    }
    public companydropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'companyName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    };



    public vechicledropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'vehicleNo',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    };

  constructor( public vechicleTransactionService:VechicleTransactionService, public formService:FormService,
               public applicationParameteConfigService: ApplicationParameteConfigService,
               public shiftTypeServiceService:ShiftTypeServiceService,
               public vechicleTypeServiceService:VechicleTypeServiceService,
               public assetService:AssetService) {

      this.pagination = {
          'currentPage': 1,
          'totalPageCount': 0,
          'recordsPerPage': 12
      };
  }

    submitTransactionData() {
        let saveTransactionDetails={}
        saveTransactionDetails['vechicleNo']=this.selectedVechicleId
        saveTransactionDetails['TSno']=this.vechicleTranctionFromDetails.TSno
        saveTransactionDetails['vehicleMode']=this.vechicleTranctionFromDetails.vehicleMode
        saveTransactionDetails['bookedBy']=this.vechicleTranctionAdditionalFromDetails.bookedBy
        saveTransactionDetails['totalDay']=this.vechicleTranctionAdditionalFromDetails.totalDay
        saveTransactionDetails['totalHrs']=this.vechicleTranctionAdditionalFromDetails.totalHrs
        saveTransactionDetails['totalKms']=this.vechicleTranctionAdditionalFromDetails.totalKms
        saveTransactionDetails['tripDate']=this.transactionDate
        saveTransactionDetails['CompanyId']=this.selectedCompanyId
        console.log("saveTransactionDetails--000000000000------------------------------")
        console.log(saveTransactionDetails)
        this.vechicleTransactionService.saveVechicleTransaction(saveTransactionDetails)
            .subscribe((savedAssetValue: any) => {
                console.log("***savedAssetValue***********---------this.formConfigData----on submit")
                console.log(savedAssetValue)
                this.getTransactionBasedOnrange(0, 10)

            })

    }


  ngOnInit() {

      this.getTransactionTotalCountForPagination()
      this.getTransactionBasedOnrange(0, 10)

  }


  getIncialDropDownValues(){
      this.vechicleTranctionFromDetails={
          TSno:"",
          vehicleMode:"",
          tripDate:new Date()
      }

      this.vechicleTranctionAdditionalFromDetails={
          bookedBy:"",
          totalDay:0,
          totalHrs:0,
          totalKms:0
      }
   this.getAllVechicleAsset()
   this.getAllCompanyAsset()
  /*  this.getAllShiftType();
    this.getAllVechicleType();*/
}

    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                console.log("******vechicleList*************");
                console.log(vechicleList)
                this.vechicleDropdownList =vechicleList
            })
    }

    getAllCompanyAsset(){
        this.assetService.getAssetlistSpecificList("Company")
            .subscribe((companyList: any) => {

                this.companyDropdownList =companyList
            })
    }
    getAllShiftType(){
        this.shiftTypeServiceService.getShiftTypeCostDropDown()
            .subscribe((shiftTypes: any) => {

                this.shiftTypesDropDown=shiftTypes
            })
    }

    onvehicleSelect (item:any) {

        console.log(item._id)
        this.selectedVechicleId=item._id
        this.assetService.getAssetDetailsByID(item._id)

            .subscribe((assetData: any) => {
                console.log("******vechicleList*************");
                console.log(assetData)
                this.vechicleTranctionFromDetails= {
                    TSno: assetData[0]['tsNo'],
                    vehicleMode: assetData[0]['vehicleType'],
                    tripDate:new Date()

                }
                console.log("******vechicleList*************");
                console.log(this.vechicleTranctionFromDetails)

            })
    }


    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }


    onCompanySelect (item:any) {
        console.log(item);
        this.selectedCompanyId=item._id
    }


    onCompanySelectAll (items: any) {
        console.log(items);
    }
    onCompanyDeSelect(items:any){
        console.log(items)
    }
    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                this.vechicleTypesDropDown=vechicleTypes
            })
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("VehicleTransactionType")
            .subscribe((data: any) => {
                console.log("confog datae09000934-02943-0")
                console.log(data)
                this.mainAsset = false;
                this.assetConfigName = "VehicleTransactionType";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }


    savedAssetCardToAssetList(savedAssetValue) {
        // this.showMessagePopup('savedAssetSuccessfully');
        //this.selectedAssets = [];
        //this.getAssetTotalCountForPagination();
        //this.getFirstFewAssets();
        this.getTransactionBasedOnrange(0, this.pagination.recordsPerPage);
        this.getTransactionTotalCountForPagination();
    }

    getTransactionTotalCountForPagination() {
        this.vechicleTransactionService.getVechicleTransactionCount()
            .subscribe((userCount: any) => {
                this.pagination.totalPageCount = userCount.count;
            });
    }

    getTransactionBasedOnrange(skip, limit) {
        this.vechicleTransactionService.getAllVechicleTransaction(skip, limit)
            .subscribe(allAssets => {
                this.allTransactionList=allAssets


            });
    }

    deleteTransactionConfiguration(assetFuelDetails) {

        this.vechicleTransactionService.deleteVechicleTransactionByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getTransactionBasedOnrange(0, 10)
            });
    }



    updateAssetCardToAssetList(editAssetValue) {
        this.getTransactionBasedOnrange(0, 10)
    }

    getEditConfiguration(assetDetails) {
        this.getAllVechicleAsset();
        this.assetId = assetDetails._id
        this.vechicleTransactionService.getVechicleTransactionByMongodbId(assetDetails._id).subscribe((data: any) => {
           console.log("datadatadatadatadata000000000000000")
           console.log(data)
            this.editTransactionDetails['TSno']=data.TSno
            this.editTransactionDetails['vechicleNo']=data.vechicleNo
            this.editTransactionDetails['vehicleMode']=data.vehicleMode
            this.editTransactionDetails['bookedBy']=data.bookedBy
            this.editTransactionDetails['totalDay']=data.totalDay
            this.editTransactionDetails['totalHrs']=data.totalHrs
            this.editTransactionDetails['totalKms']=data.totalKms
            this['selectedItems']="test"
        })

    }

    updateTransactionDetails() {
        this.vechicleTransactionService.updateVechicleTransactionById(this.assetId, this.editTransactionDetails)
            .subscribe((assetvalues: any) => {

                this.getTransactionBasedOnrange(0, 10)
            })

    }


}
