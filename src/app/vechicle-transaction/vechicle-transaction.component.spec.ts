import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VechicleTransactionComponent } from './vechicle-transaction.component';

describe('VechicleTransactionComponent', () => {
  let component: VechicleTransactionComponent;
  let fixture: ComponentFixture<VechicleTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VechicleTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VechicleTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
