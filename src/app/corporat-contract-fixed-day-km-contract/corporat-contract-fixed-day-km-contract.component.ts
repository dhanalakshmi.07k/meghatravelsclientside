import { Component, OnInit } from '@angular/core';
import {CctransactionService} from "../../services/cctransaction.service";
import {AssetService} from "../../services/asset.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";
import {VehicleCorporateContractService} from "../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../services/corporate-contract-fields.service";
import * as _ from "lodash";
import * as XLSX from "xlsx";
import {ExcelService} from "../../services/excel.service";


@Component({
  selector: 'app-corporat-contract-fixed-day-km-contract',
  templateUrl: './corporat-contract-fixed-day-km-contract.component.html',
  styleUrls: ['./corporat-contract-fixed-day-km-contract.component.scss']
})
export class CorporatContractFixedDayKmContractComponent implements OnInit {
    bulkUpload:any={}
    public vechicleTransactionSelected = "";
    public selectedVehicleItems = "";
    public companySelected = "";
    public CC4List: any = [];
    transactionDate=new Date().toJSON().slice(0,10);
    companyAssetDropDown: any;
    corporateContractType: any = "fixedDayKmContract"
    vechicleDropdownList: any = [];
    corporateContractFields: any;
    companyTransactionSelected: any = {}
    formConfigData: any = {};
    companySelectedDetails = ""
    public vechicleSelectedVehicleType = "";
    public vechicleSelectedDetails = {};
    companyDropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'companySelectedForDropDown',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection: true
    }
    vechicledropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'vehicleNumber',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
    };

    constructor(public cctransactionService: CctransactionService,
                private assetService: AssetService,
                private vechicleTypeServiceService: VechicleTypeServiceService,
                private vehicleCorporateContractService: VehicleCorporateContractService,
                private corporateContractFieldsService: CorporateContractFieldsService,
                private excelService:ExcelService) {
    }

    getIncialConfig() {
        this.selectedVehicleItems = ""
        this.companySelectedDetails = ""
        this.vechicleSelectedVehicleType = ''
        this.formConfigData = []
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
    }

    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.formConfigData=[];
        this.companyTransactionSelected=""
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }




    getAllCompanyOfTypeCC4() {
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                this.companyAssetDropDown = _.uniqBy(companyList, function (companyList) {
                    return companyList['companySelectedForDropDown']
                });

                /*this.companyAssetDropDown =companyList*/
            })
    }

    getAllVechicleAsset() {
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList = vechicleList;

            })
    }

    getCCTransaction() {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CC4List = CCListDetails
            });
    }


    onvehicleSelect(vehicleDetails) {
        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {
                this.vechicleSelectedDetails = vehicleDetails
                this.vechicleSelectedVehicleType = vehicleDetails['vehicleType']

            })
    }


    onCompanySelect(companyDetails) {
        this.companyTransactionSelected = companyDetails
        this.vehicleCorporateContractService.getVehicleCorporateContractByMongodbId(companyDetails['_id'])
            .subscribe((companyCC: any) => {
                this.vehicleCorporateContractService.getcompanyCCByMongodbIdAndCCTypeAndvehicleType(companyCC[0].companySelected._id,
                    this.corporateContractType, this.vechicleSelectedVehicleType)
                    .subscribe((companyCCDetails: any) => {
                        if (companyCCDetails && companyCCDetails[0]) {
                            companyCCDetails = companyCCDetails[0];
                            for (var g = 0; g < this.corporateContractFieldsService.corporateContractTypes.length; g++) {
                                if (this.corporateContractFieldsService.corporateContractTypes[g]['assetType']['typeId'] ==
                                    companyCCDetails.corporateContract) {
                                    let CCFieldsConfiguration = Object.keys(this.corporateContractFieldsService.corporateContractTypes[g].configuration);
                                    let companyCCKeys = Object.keys(companyCCDetails);
                                    let VehicleCCKeys = Object.keys(this.vechicleSelectedDetails);
                                    if (companyCCKeys) {
                                        for (var k = 0; k < companyCCKeys.length; k++) {
                                            for (let p = 0; p < CCFieldsConfiguration.length; p++) {
                                                if (companyCCKeys[k] == CCFieldsConfiguration[p]) {
                                                    this.formConfigData
                                                        [this.corporateContractFieldsService.corporateContractTypes[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                        companyCCDetails[companyCCKeys[k]]
                                                }
                                            }
                                        }

                                        this.formConfigData['vehicleType'] = this.vechicleSelectedVehicleType['vehicleTypeName'];

                                    }
                                    console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                    this.fromConfigObjectForFrom(this.corporateContractFieldsService.corporateContractTypes[g], companyCCDetails)
                                }

                            }
                        }
                        else {
                            alert("selected vehicle type  is not configured")
                        }
                    });
            })


    }

    fromConfigObjectForFrom(configDetailsObj, companyCCDetails) {
        let configObj = configDetailsObj.configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for (var prop of responseKeys) {
            formData.push(configObj[prop]);
        }
        if (formData) {
            this.corporateContractFields = formData;
        }


    }

    deleteCC1Trasnaction(ccDetails) {

        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction()
            })
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }



    submitTransaction() {

        var obj = {}
        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
        obj['transactionEndDate']=this.bulkUpload.transactionEndDate
        obj['noOfKms'] = this.formConfigData['noOfKms']
        obj['noOfDays'] = this.formConfigData['noOfDays']
        obj['minDaysPerMonth'] = this.formConfigData['minDaysPerMonth']
        obj['fixedAmount'] = this.formConfigData['fixedAmount']
        obj['ratePerDay'] = this.formConfigData['ratePerDay']
        obj['ratePerKM'] = this.formConfigData['ratePerKM']
        obj['vehicleType'] =this.vechicleSelectedDetails['vehicleType']
        obj['vehicleNumber'] = this.vechicleSelectedDetails['vehicleNumber']
        obj['companyDetails'] = this.companyTransactionSelected['companySelectedForDropDown']
        obj['corporateContract'] = this.corporateContractType
        //based on only kmts
        if(this.formConfigData['noOfKms']){
            obj['extraKms'] =Math.abs( this.formConfigData['minKMsPerMonth'] - this.formConfigData['noOfKms'])
            console.log(obj['ratePerKM'] )
            obj['totalAmt'] = (obj['ratePerKM'] * obj['extraKms']) +parseInt( this.formConfigData['fixedAmount'])
        }
        //based on only kmts and days
        if(this.formConfigData['noOfDays'] &&this.formConfigData['noOfKms'] ){
            obj['extraKms'] = Math.abs(this.formConfigData['minKMsPerMonth'] - this.formConfigData['noOfKms'])
            obj['extraDays'] =Math.abs( this.formConfigData['minDaysPerMonth'] - this.formConfigData['noOfDays'])
            obj['totalAmt'] = (this.formConfigData['ratePerKm'] * obj['extraKms'])
                +parseInt( this.formConfigData['fixedAmount'])+(this.formConfigData['ratePerDay']* obj['extraDays'])
        }
        //based on only days
        if(this.formConfigData['noOfDays']){
            obj['extraDays'] =  Math.abs(this.formConfigData['minDaysPerMonth'] - this.formConfigData['noOfDays'])
            obj['totalAmt'] = (this.formConfigData['ratePerDay'] *obj['extraDays']) + parseInt(this.formConfigData['fixedAmount'])
        }
        this.cctransactionService.saveCCTransaction(obj)
            .subscribe((data: any) => {
                this.getCCTransaction()
            });


    }


    listData:any
    listIntermediateData:any
    tableDataObj:any=[];
    cc1ExcelDetails:any;
    uploadCc1ExcelDetails(event){
        let that=this
        let tableObjs;
        let input = event   .target;
        let reader = new FileReader();
        reader.onload = function(){
            let fileData = reader.result;
            let wb = XLSX.read(fileData, {type : 'binary'});
            wb.SheetNames.forEach(function(sheetName){
                let rowObj =XLSX.utils.sheet_to_json(wb.Sheets[sheetName]);
                let tableObjs=JSON.stringify(rowObj);

                if (tableObjs == undefined || tableObjs== null){
                    return;
                }
                else{
                    console.log(JSON.stringify(rowObj))
                    that.listData=JSON.stringify(rowObj)
                   that.saveIntermediate(that.listData)
                }
            })
        };
        reader.readAsBinaryString(input.files[0])


    }
    saveIntermediate(listData){
        console.log("intermediate")
        this.listIntermediateData=listData
    }


    saveCcCommomDetaisToDB(){
        var arrayListData = JSON.parse("[" + this.listIntermediateData + "]");
        for(let p=0;p<arrayListData[0].length;p++){
            this.vehicleCorporateContractService.getcompanyCCByCompanyAndCCTypeAndVehicleType(
                arrayListData[0][p]['company'],this.corporateContractType
                ,arrayListData[0][p]['mode'])
                .subscribe((companyCCDetails: any) =>{
                    console.log("companyCCDetails------------------")
                    console.log(companyCCDetails[0])
                    if(companyCCDetails[0]){
                        var obj = {}
                        obj['transactionStartDate']=this.bulkUpload.transactionStartDate
                        obj['transactionEndDate']=this.bulkUpload.transactionEndDate
                        obj['noOfKms'] = arrayListData[0][p]['kMsPerDay']
                        obj['noOfDays'] = arrayListData[0][p]['totalDays']
                        obj['minDaysPerMonth'] = companyCCDetails[0]['minDaysPerMonth']
                        obj['fixedAmount'] = companyCCDetails[0]['fixedAmount']
                        obj['minKMsPerMonth'] = companyCCDetails[0]['minKMsPerMonth']
                        obj['ratePerDay'] = companyCCDetails[0]['ratePerDay']
                        obj['ratePerKM'] = companyCCDetails[0]['ratePerKM']
                        obj['vehicleType'] = arrayListData[0][p]['mode']
                        obj['vehicleNumber'] = arrayListData[0][p]['vehicleNumber']
                        obj['companyDetails'] = arrayListData[0][p]['company']
                        obj['corporateContract'] = this.corporateContractType

                        //based on only kmts
                        if(arrayListData[0][p]['kMsPerDay']!=0 && arrayListData[0][p]['totalDays']!==0 && obj['noOfKms']>obj['minKMsPerMonth']){
                            obj['extraKms'] =Math.abs( obj['minKMsPerMonth'] - obj['noOfKms'])
                            console.log("inside first ifffffffffff" )
                            obj['totalAmt'] = (obj['ratePerKM'] * obj['extraKms']) + parseInt(obj['fixedAmount'])
                        }

                        if(arrayListData[0][p]['kMsPerDay']!=0 && arrayListData[0][p]['totalDays']!==0 && obj['noOfKms']<obj['minKMsPerMonth']){
                            obj['totalAmt'] = parseInt(obj['fixedAmount'])
                        }
                        //based on only kmts and days
                       /* if(obj['noOfDays'] &&obj['noOfKms'] ){
                            obj['extraKms'] = Math.abs(obj['minKMsPerMonth'] - obj['noOfKms'])
                            obj['extraDays'] =Math.abs( obj['minDaysPerMonth'] - obj['noOfDays'])
                            obj['totalAmt'] = (obj['ratePerKm'] * obj['extraKms'])
                                + obj['fixedAmount']+(obj['ratePerDay']* obj['extraDays'])
                        }*/
                        //based on only days
                        if(arrayListData[0][p]['totalDays']===0 && (arrayListData[0][p]['kMsPerDay']>companyCCDetails[0]['minKMsPerMonth'])){
                            console.log("inside Second ifffffffffff" )
                            obj['extraKms'] =  Math.abs(companyCCDetails[0]['minKMsPerMonth'] - arrayListData[0][p]['kMsPerDay'])
                            obj['totalAmt'] = (obj['ratePerKM'] *obj['extraKms']) + parseInt(obj['fixedAmount'])
                            console.log(obj['totalAmt'])
                        }

                        if(arrayListData[0][p]['totalDays']===0 && (arrayListData[0][p]['kMsPerDay']<companyCCDetails[0]['minKMsPerMonth'])){
                            console.log("inside Second ifffffffffff" )
                            obj['totalAmt'] = parseInt(obj['fixedAmount'])
                            console.log(obj['totalAmt'])
                        }

                        this.cctransactionService.saveCCTransaction(obj)
                            .subscribe((data: any) => {
                                this.getCCTransaction()
                            });

                    }

                    else{
                        alert(arrayListData[0][p]['company']+"is not configured of type"+arrayListData[0][p]['mode']);
                    }
                })

        }

    }

    data: any = [{
        SlNo: 0,
        company:"",
        vehicleNumber: "",
        mode:"",
        totalDays: 0,
        kMsPerDay:0,
        gps:0
    }];

    exportAsXLSX():void {
        this.excelService.exportAsExcelFile(this.data, 'FixedDayKMs');
    }


    ngOnInit() {
        this.getAllCompanyOfTypeCC4();
        this.getCCTransaction();
        this.transactionDate=new Date().toJSON().slice(0,10);
    }

}
