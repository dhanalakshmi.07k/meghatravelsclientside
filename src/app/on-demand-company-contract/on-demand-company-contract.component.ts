import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-on-demand-company-contract',
  templateUrl: './on-demand-company-contract.component.html',
  styleUrls: ['./on-demand-company-contract.component.scss']
})
export class OnDemandCompanyContractComponent implements OnInit {
    constructor(private router: Router) {
    }
    navigateOnDemand() {
        this.router.navigate(['onDemand']);
    }
    navigateOnDemandShiftType() {
        this.router.navigate(['OnDemandShiftType']);
    }

    ngOnInit() {
    }


}
