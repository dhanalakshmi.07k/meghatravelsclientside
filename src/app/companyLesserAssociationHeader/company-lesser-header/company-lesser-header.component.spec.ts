import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyLesserHeaderComponent } from './company-lesser-header.component';

describe('CompanyLesserHeaderComponent', () => {
  let component: CompanyLesserHeaderComponent;
  let fixture: ComponentFixture<CompanyLesserHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyLesserHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyLesserHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
