import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-lesser-card',
    templateUrl: './lesser-card.component.html',
    styleUrls: ['./lesser-card.component.scss']
})
export class LesserCardComponent implements OnInit {
    @Input() assetData: any;

    constructor() {
    }

    ngOnInit() {
    }

}
