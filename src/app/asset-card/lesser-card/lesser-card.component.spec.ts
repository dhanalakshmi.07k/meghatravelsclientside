import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {LesserCardComponent} from './lesser-card.component';

describe('LesserCardComponent', () => {
    let component: LesserCardComponent;
    let fixture: ComponentFixture<LesserCardComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LesserCardComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LesserCardComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
