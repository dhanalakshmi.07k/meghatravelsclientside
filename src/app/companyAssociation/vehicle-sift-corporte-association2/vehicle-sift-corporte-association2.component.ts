import { Component, OnInit } from '@angular/core';
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {AssetService} from "../../../services/asset.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {Router} from "@angular/router";
import {ShiftTypeServiceService} from "../../../services/shift-type-service.service";

@Component({
  selector: 'app-vehicle-sift-corporte-association2',
  templateUrl: './vehicle-sift-corporte-association2.component.html',
  styleUrls: ['./vehicle-sift-corporte-association2.component.scss']
})
export class VehicleSiftCorporteAssociation2Component implements OnInit {

    allVechicleTypeDropDown:any;
    formConfigData:any={};
    term:any;
    vechicleDropdownList:any;
    lesserDropdownList:any;
    shiftTypeDropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'ShiftTypeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    isEdit:any=false
    corporateContractType="Shift Type";
    shiftSelectedDetails:any;
    corporateContractFields:any=[];
    corporateContractDetailsList:any=[];
    currentCompanySelected:string;
    companyAssetDropDown:any;
    companySelected:any;
    vehicleTypeDropdownSettings:any={};
    vehicleTypeSelected:any;
    shiftTypeSelected:any;
    public pagination: any;
    public shiftTypesDropDown:any=[]
    public skip: number;
    public limit: number;
    public companyDropdownSettings = {};

    public vehicleDropdownSettings = {};

    public lesserDropdownSettings = {};


    constructor(public vechicleTypeServiceService:VechicleTypeServiceService,
                public corporateContractFieldsService:CorporateContractFieldsService,
                public assetService:AssetService,
                public vehicleCorporateContractService:VehicleCorporateContractService,
                private router: Router,public shiftTypeServiceService:ShiftTypeServiceService) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
        this.corporateContractType="Shift Type"

    }

    onCompanySelectSelect(item: any) {

        this.currentCompanySelected=item;
        console.log(this.currentCompanySelected);

    }
    onShiftSelect(shiftDetails){
        this.shiftSelectedDetails= shiftDetails;
    }

    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }
    submitVehicleCorporateContractDetails(){
        this.formConfigData['companySelectedForDropDown']= this.companySelected[0].companyName
        this.formConfigData['companySelected']= this.companySelected[0]
        if(this.vehicleTypeSelected){
            this.formConfigData['vehicleTypeSelected']= this.vehicleTypeSelected[0]
        }
        this.formConfigData['shiftTypeSelected']= this.shiftSelectedDetails
        this.formConfigData['corporateContract']= this.corporateContractType
        this.vehicleCorporateContractService.checkCCExitsBasedOnVehicleTypeAndName(this.companySelected[0].companyName
                ,this.corporateContractType,this.formConfigData['vehicleTypeSelected']['vehicleTypeName'] )
            .subscribe((existResponse: any) => {
                if(existResponse.length==0){
                    this.vehicleCorporateContractService.saveVehicleCorporateContract( this.formConfigData  )
                        .subscribe((ccCount: any) => {
                            this.getCorporateContractByType(this.corporateContractType,0,10)
                            this.getCorporateContractCount();
                        });
                }
                else{
                    alert(this.companySelected[0].companyName + "is already configured to shift Type" + this.formConfigData['vehicleTypeSelected']['vehicleTypeName'])
                }
            })

    }
    getFromSetUpDetails(){
        this.formConfigData={}
        this.companySelected={};
        this.companySelected={};
        this.vehicleTypeSelected={}
        this.shiftTypeSelected={}
        this.companyDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.vehicleTypeDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'vehicleTypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)

    }
    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.allVechicleTypeDropDown=vechicleTypes
            })
    }

    populateCorporateContractType(){
        this.formConfigData={}
        this.companySelected=""
        this.vehicleTypeSelected=""
        this.shiftTypeSelected=""
        this.isEdit=false;
        this.companyDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.vehicleTypeDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'vehicleTypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.getAllVechicleType()
        this.getAllCompanyAsset();
        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }
            else{
                this.corporateContractFields=[]
            }
        }

    }

    deleteExpenseDetails(CCDetails){
        this.vehicleCorporateContractService.deleteVehicleCorporateContractByMongodbId(CCDetails['_id'])
            .subscribe(expenseData => {

                this.getCorporateContractTotalCountForPagination();
                this.getCorporateContractBasedOnrange(0,10)
            });
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }
    getCorporateContractTotalCountForPagination() {
        this.vehicleCorporateContractService.getVehicleCorporateContractCount()
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }
    getCorporateContractBasedOnrange(skip, limit) {
        this.vehicleCorporateContractService.getAllVehicleCorporateContractList()
            .subscribe((corporateContractDetails:any) => {
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }

    getAllLesserAsset(){
        this.assetService.getAssetlistSpecificList("Lesser")
            .subscribe((lesserList: any) => {
                this.lesserDropdownList =lesserList
            })
    }

    getAllCompanyAsset(){
        this.assetService.getAssetlistSpecificList("Company")
            .subscribe((companyList: any) => {
                this.companyAssetDropDown =companyList
            })
    }



    getCorporateContractByType(ccType,skip,limit) {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeRange(ccType,skip,limit)
            .subscribe((corporateContractDetails:any) => {
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }

    getCorporateContractCount() {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeCount(this.corporateContractType)
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        }
    }



    getAllShiftType(){
        this.shiftTypeServiceService.getShiftTypeCostDropDown()
            .subscribe((shiftTypes: any) => {
                console.log(shiftTypes)
                this.shiftTypesDropDown=shiftTypes
            })
    }


    editCorporateContract(corporateContractDetails){
        this.isEdit=true
        this.formConfigData['agreementStartDate']=corporateContractDetails['agreementStartDate']
        this.formConfigData['agreementEndDate']=corporateContractDetails['agreementEndDate']
        this.formConfigData=corporateContractDetails

    }

    updateVehicleCorporateContractDetails(){
        this.vehicleCorporateContractService.updateVehicleCorporateContractById(this.formConfigData['_id'],this.formConfigData)
            .subscribe(expenseData => {
                console.log( expenseData)
            });
    }

    ngOnInit() {
        this.getCorporateContractByType(this.corporateContractType,0,10)
        this.getCorporateContractCount();
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
        this.populateCorporateContractType()
        this.getAllShiftType()

    }



}
