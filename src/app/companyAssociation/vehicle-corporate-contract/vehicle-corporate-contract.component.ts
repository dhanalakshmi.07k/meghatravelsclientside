import { Component, OnInit } from '@angular/core';
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {ShiftTypeServiceService} from "../../../services/shift-type-service.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {AssetService} from "../../../services/asset.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-vehicle-corporate-contract',
    templateUrl: './vehicle-corporate-contract.component.html',
    styleUrls: ['./vehicle-corporate-contract.component.scss']
})
export class VehicleCorporateContractComponent implements OnInit {


    corporateContractDetails:any={}
    allVechicleTypeDropDown:any;
    formConfigData:any={};
    term:any;
    vechicleDropdownList:any;
    corporateContractType='fixedDayKmContract';
    corporateContractFields:any=[];
    corporateContractDetailsList:any=[];
    currentCompanySelected:string;
    companyAssetDropDown:any;
    companySelected:any;
    vehicleTypeSelected:any;
    public pagination: any;
    vehicleConfigList:any=[]
    isEdit=false
    editField: string;
    public skip: number;
    public limit: number;
    companyDropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'companyName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    constructor(public vechicleTypeServiceService:VechicleTypeServiceService,
                public shiftTypeServiceService:ShiftTypeServiceService,
                public corporateContractFieldsService:CorporateContractFieldsService,
                public assetService:AssetService,
                public vehicleCorporateContractService:VehicleCorporateContractService,
                private router: Router) {
        this.corporateContractType='fixedDayKmContract'

        this.corporateContractDetails["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.corporateContractDetails["agreementEndDate"]=new Date().toJSON().slice(0,10)
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };

    }
    onCompanySelectSelect(item: any) {
        this.currentCompanySelected=item;
        console.log(this.currentCompanySelected);

    }

    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }
    getFormIncialConfig(){
        this.getVechicleType();
        this.showCorporateContractType();
        this.getAllCompanyAsset();
        this.companyDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }

    }
    getFromCleanUpDetails(){
        this.corporateContractType='fixedDayKmContract'
        this.corporateContractDetails["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.corporateContractDetails["agreementEndDate"]=new Date().toJSON().slice(0,10)
        this.vehicleTypeSelected=""
        this.companySelected=[];
        this.vehicleTypeSelected={}
        this.formConfigData={};

    }
    showCorporateContractType(){
        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }
            else{
                this.corporateContractFields=[]
            }
        }
    }
    deleteExpenseDetails(CCDetails){
        this.vehicleCorporateContractService.deleteVehicleCorporateContractByMongodbId(CCDetails['_id'])
            .subscribe(expenseData => {
                this.getCorporateContractByType(this.corporateContractType,0,10)
                this.getCorporateContractCount();
            });
    }

deleteTransactionData:any
intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }

    getCorporateContractTotalCountForPagination() {
        this.vehicleCorporateContractService.getVehicleCorporateContractCount()
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }
    getAllCompanyAsset(){
        this.assetService.getAssetlistSpecificList("Company")
            .subscribe((companyList: any) => {
                this.companyAssetDropDown =companyList
            })
    }

    getVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {

                this.allVechicleTypeDropDown=vechicleTypes
                this.formVehicleTypeTable(vechicleTypes)
            })
    }
    formVehicleTypeTable(vechicleTypes){
            let configTableList: Array<any> = []
        for(let k=0;k<vechicleTypes.length;k++){

            configTableList.push(  { id: k, vehicleType: vechicleTypes[k]['vehicleTypeName'],
                minDaysPerMonth: 0, minKMsPerMonth: 0, fixedAmount: 0, ratePerKM: 0,ratePerDay:0 })

        }
        this.vehicleConfigList=configTableList
        }
    getFromSetUpDetails(){
        this.formConfigData={}
        this.companySelected=[];
        this.vehicleTypeSelected={}
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
    }
    updateList(id: number, property: string, event: any) {
        const editField = event.target.textContent;
        this.vehicleConfigList[id][property] = editField;

    }
    saveCorporateContract(vehicleConfigDetails){
        this.formConfigData= vehicleConfigDetails
        this.formConfigData['agreementStartDate']= this.corporateContractDetails.agreementStartDate
        this.formConfigData['agreementEndDate']= this.corporateContractDetails.agreementEndDate
        this.formConfigData['corporateContract']= 'fixedDayKmContract'
        this.formConfigData['companySelected']= this.companySelected[0]
        this.formConfigData['companySelectedForDropDown']= this.companySelected[0]['companyName']
        console.log("this.formConfigData--this.companySelected[0].companyName-----")
        console.log(vehicleConfigDetails['vehicleType'])
        console.log(this.companySelected[0].companyName)
        this.vehicleCorporateContractService.checkCCExitsBasedOnVehicleTypeAndName(
            this.companySelected[0].companyName,this.corporateContractType,vehicleConfigDetails['vehicleType'] )
            .subscribe((existResponse: any) => {
                console.log("**************formConfigData****checkCCExitsBasedOnVehicleTypeAndNamessssss*************");
                console.log( existResponse)
                if(existResponse.length==0){
                    this.vehicleCorporateContractService.saveVehicleCorporateContract(this.formConfigData)
                        .subscribe((ccCount: any) => {
                            this.getCorporateContractByType(this.corporateContractType,0,10)
                            this.getCorporateContractCount();
                        });

                }
                else{
                    alert(this.companySelected[0].companyName + "is already configured for type " + vehicleConfigDetails['vehicleType'])
                }
            });


    }
    changeValue(id: number, property: string, event: any) {
        this.editField = event.target.textContent;

    }
    editCorporateContract(corporateContractDetails){
        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }
            else{
                this.corporateContractFields=[]
            }
        }
        console.log( corporateContractDetails['companyName'])
        this.isEdit=true
        this.formConfigData['agreementStartDate']=corporateContractDetails['agreementStartDate']
        this.formConfigData['agreementEndDate']=corporateContractDetails['agreementEndDate']
        this.formConfigData=corporateContractDetails

    }
    updateVehicleCorporateContractDetails(){
        console.log("**************formConfigData*****************");
        console.log( this.formConfigData)
        this.vehicleCorporateContractService.updateVehicleCorporateContractById(this.formConfigData['_id'],this.formConfigData)
            .subscribe(expenseData => {
                console.log("**************update message*****************");
                console.log( expenseData)
            });
    }
    getCorporateContractByType(ccType,skip,limit) {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeRange(ccType,skip,limit)
            .subscribe((corporateContractDetails:any) => {
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }

    getCorporateContractCount() {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeCount(this.corporateContractType)
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        }
    }

    ngOnInit() {
        this.corporateContractType='fixedDayKmContract'
        this.getCorporateContractByType(this.corporateContractType,0,10)
        this.getCorporateContractCount();
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
    }




}
