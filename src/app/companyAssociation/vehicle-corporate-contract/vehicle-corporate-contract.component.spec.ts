import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleCorporateContractComponent } from './vehicle-corporate-contract.component';

describe('VehicleCorporateContractComponent', () => {
  let component: VehicleCorporateContractComponent;
  let fixture: ComponentFixture<VehicleCorporateContractComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleCorporateContractComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleCorporateContractComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
