import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OnDemandAssociationComponent } from './on-demand-association.component';

describe('OnDemandAssociationComponent', () => {
  let component: OnDemandAssociationComponent;
  let fixture: ComponentFixture<OnDemandAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OnDemandAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OnDemandAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
