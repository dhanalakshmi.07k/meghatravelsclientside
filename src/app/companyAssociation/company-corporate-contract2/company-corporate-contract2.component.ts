import { Component, OnInit } from '@angular/core';
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";
import {AssetService} from "../../../services/asset.service";
import {VehicleCorporateContractService} from "../../../services/vehicle-corporate-contract.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-company-corporate-contract2',
  templateUrl: './company-corporate-contract2.component.html',
  styleUrls: ['./company-corporate-contract2.component.scss']
})
export class CompanyCorporateContract2Component implements OnInit {

    allVechicleTypeDropDown:any;
    formConfigData:any={};
    term:any;
    vechicleDropdownList:any;
    lesserDropdownList:any;
    corporateContractType="contract type 2";
    corporateContractFields:any=[];
    corporateContractDetailsList:any=[];
    currentCompanySelected:string;
    companyAssetDropDown:any;
    deleteTransactionData:any
    companySelected:any;
    vehicleTypeSelected:any;
    public pagination: any;
    isEdit=false
    companyDropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'companyName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }
    vehicleTypeDropdownSettings = {
        singleSelection: true,
        idField: '_id',
        textField: 'vehicleTypeName',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection:true
    }

    public skip: number;
    public limit: number;

    constructor(public vechicleTypeServiceService:VechicleTypeServiceService,
                public corporateContractFieldsService:CorporateContractFieldsService,
                public assetService:AssetService,
                public vehicleCorporateContractService:VehicleCorporateContractService,
                private router: Router) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
        this.corporateContractType="contract type 2"
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)

    }

    onCompanySelectSelect(item: any) {

        this.currentCompanySelected=item;
        console.log(this.currentCompanySelected);

    }

    onSelectAll (items: any) {
        console.log(items);
    }
    onItemDeSelect(items:any){
        console.log(items)
    }
    submitVehicleCorporateContractDetails(){
        this.formConfigData['companySelectedForDropDown']= this.companySelected[0].companyName
        this.formConfigData['companySelected']= this.companySelected[0]
        if(this.vehicleTypeSelected){
            this.formConfigData['vehicleTypeSelected']= this.vehicleTypeSelected[0]
            this.formConfigData['vehicleType']= this.formConfigData['vehicleTypeSelected']['vehicleTypeName']
        }
        this.formConfigData['corporateContract']= this.corporateContractType
        if(this.formConfigData['fixedCostDoubleDriver'] && this.formConfigData['fixedCostDoubleDriver'] ){
            alert (" fixed cost has  to be fixedCostDoubleDriver or fixedCostDoubleDriver")
        }
        else{
            this.vehicleCorporateContractService.checkCCExitsBasedOnVehicleTypeAndName( this.companySelected[0].companyName,this.corporateContractType,this.formConfigData['vehicleType'] )
                .subscribe((existResponse: any) => {

                    if(existResponse.length==0){
                        this.vehicleCorporateContractService.saveVehicleCorporateContract( this.formConfigData )
                            .subscribe((ccCount: any) => {
                                this.getFromSetUpDetails()
                                this.getCorporateContractByType(this.corporateContractType,0,10)
                            });
                    }
                    else{
                        alert(this.companySelected[0].companyName + "is already configured for type  "+ this.formConfigData['vehicleTypeSelected']['vehicleTypeName'])
                    }
                });
        }




    }

    editCorporateContract(corporateContractDetails){

        console.log( corporateContractDetails)
        this.isEdit=true
        this.formConfigData['agreementStartDate']=corporateContractDetails['agreementStartDate']
        this.formConfigData['agreementEndDate']=corporateContractDetails['agreementEndDate']
        this.formConfigData=corporateContractDetails
        console.log( this.formConfigData)

        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){

            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }

        }
        console.log( this.corporateContractFields)
    }
    getFromSetUpDetails(){
        this.formConfigData={}
        this.companySelected=[];
        this.vehicleTypeSelected={}
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)
    }
    getAllVechicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)
                this.allVechicleTypeDropDown=vechicleTypes
            })
    }



    companyOnDemandAssoication(){
        this.router.navigate(['companyOnDemandAssoication']);
    }
    populateCorporateContractType(){
        this.formConfigData={}
        this.isEdit=false;
        this.companyDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'companyName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this. vehicleTypeDropdownSettings = {
            singleSelection: true,
            idField: '_id',
            textField: 'vehicleTypeName',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 1,
            allowSearchFilter: true,
            closeDropDownOnSelection:true
        }
        this.getAllVechicleType()
        this.getAllCompanyAsset();
        for(var p=0;p<this.corporateContractFieldsService.corporateContractTypes.length;p++){
            if(this.corporateContractFieldsService.corporateContractTypes[p].assetType.typeId===
                this.corporateContractType){
                let configObj=  this.corporateContractFieldsService.corporateContractTypes[p].configuration
                let responseKeys = Object.keys(configObj);
                let formData = [];
                for ( var prop of responseKeys) {
                    formData.push( configObj[prop]);
                }
                this.corporateContractFields=formData
                break
            }
            else{
                this.corporateContractFields=[]
            }
        }

    }


    updateVehicleCorporateContractDetails(){
        this.vehicleCorporateContractService.updateVehicleCorporateContractById(this.formConfigData['_id'],this.formConfigData)
            .subscribe(expenseData => {

            });
    }

    deleteCCDetails(CCDetails){
        this.vehicleCorporateContractService.deleteVehicleCorporateContractByMongodbId(CCDetails['_id'])
            .subscribe(expenseData => {
                this.getCorporateContractByType(this.corporateContractType,0,10)
            });
    }

intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }
    getCorporateContractCount() {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeCount(this.corporateContractType)
            .subscribe((ccCount: any) => {
                this.pagination.totalPageCount = ccCount.count;
            });
    }
    getCorporateContractBasedOnrange(skip, limit) {
        this.vehicleCorporateContractService.getAllVehicleCorporateContractList()
            .subscribe((corporateContractDetails:any) => {
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }
    getAllVechicleAsset(){
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                this.vechicleDropdownList =vechicleList
            })
    }

    getAllLesserAsset(){
        this.assetService.getAssetlistSpecificList("Lesser")
            .subscribe((lesserList: any) => {
                this.lesserDropdownList =lesserList
            })
    }

    getAllCompanyAsset(){
        this.assetService.getAssetlistSpecificList("Company")
            .subscribe((companyList: any) => {
                this.companyAssetDropDown =companyList
            })
    }

    getCorporateContractByType(ccType,skip,limit) {
        this.vehicleCorporateContractService.getCorporateContractByCCTypeRange(ccType,skip,limit)
            .subscribe((corporateContractDetails:any) => {
                console.log("----------typeof corporateContractDetails")
                console.log( corporateContractDetails)
                console.log(typeof corporateContractDetails)
                this.corporateContractDetailsList=corporateContractDetails.reverse()
            });
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getCorporateContractByType(this.corporateContractType,0,10)
        }
    }




    ngOnInit() {
        this.getCorporateContractByType(this.corporateContractType,0,10)
        this.getCorporateContractCount();
        this.formConfigData["agreementStartDate"]=new Date().toJSON().slice(0,10);
        this.formConfigData["agreementEndDate"]=new Date().toJSON().slice(0,10)

    }


}
