import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyCorporateContract2Component } from './company-corporate-contract2.component';

describe('CompanyCorporateContract2Component', () => {
  let component: CompanyCorporateContract2Component;
  let fixture: ComponentFixture<CompanyCorporateContract2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyCorporateContract2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyCorporateContract2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
