import {Component, OnInit} from '@angular/core';
import * as _ from "lodash";
import {AssetService} from "../../../services/asset.service";
import {VechicleTypeServiceService} from "../../../services/vechicle-type-service.service";
import {ReportServiceService} from "../../../services/reportService/report-service.service";
import {ExcelService} from "../../../services/excel.service";


@Component({
    selector: 'app-report',
    templateUrl: './report.component.html',
    styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit {
    showQueryResults:any={}

    companyAssetList:any=[]
    driverAssetList:any=[]
    lesserAssetList:any=[]
    vehicleAssetList:any=[]
    vehicleTypeList:any=[]
    resultDetails:any=[]
    selectedVehicleType:any=""
    selectCompany:any=""
    selectedCCType:any=""
    individualDate:Date=null;
    startDate:Date=null;
    endDate:Date=null;
    reportHeader:any;
    ccList:any=["OnDemand","contract type 2","Shift Type","fixedDayKmContract"]
    constructor(public assetService: AssetService, public vechicleTypeServiceService:VechicleTypeServiceService,
                public reportServiceService:ReportServiceService,private excelService:ExcelService) {
    }



    ngOnInit() {
        this.getAllVehicleType();
        this.getAlDriverAssets();
        this.getAllVendorAssets();
        this.getAllVehicleAssets();
        this.getAllAssets();
    }


    getAllVehicleType(){
        this.vechicleTypeServiceService.getAllVechicleTypeDropdown()
            .subscribe((vechicleTypes: any) => {
                console.log(vechicleTypes)

                this.vehicleTypeList=vechicleTypes
            })
    }

    getAllAssets(){
        this.assetService.getAssetlistSpecificList( "Company" )
            .subscribe((assetList: any) => {
                this.companyAssetList=assetList
            })
    }


    getAlDriverAssets(){
        this.assetService.getAssetlistSpecificList( "Driver" )
            .subscribe((assetList: any) => {
                this.driverAssetList=assetList
            })
    }

    getAllVendorAssets(){
        this.assetService.getAssetlistSpecificList( "Lesser" )
            .subscribe((assetList: any) => {
                this.lesserAssetList=assetList
            })
    }

    getAllVehicleAssets(){
        this.assetService.getAssetlistSpecificList( "Vehicle" )
            .subscribe((assetList: any) => {
                this.vehicleAssetList=assetList
            })
    }

    submitQuery(){

        this.resultDetails=[]
        console.log("this.individualDate===============")
        console.log(this.individualDate)
        if(this.individualDate!==null){

            this.reportServiceService.getIndividualDayReport(this.individualDate)
                .subscribe(
                response => {
                    this.reportHeader=Object.keys(response[0])
                    this.resultDetails=response
                    console.log(response)
                    this.showQueryResults.individualDate=this.individualDate
                    this.individualDate=null

                },
                err => {
                    this.resultDetails=[]
                    alert("there is a issue in selection of query please check")
                }
            );
        }




      else  if(this.startDate!==null && this.endDate!==null  ){
            this.reportServiceService.getRangeByReport(this.startDate,this.endDate)
                .subscribe(
                    response => {
                        this.reportHeader=Object.keys(response[0])
                        this.resultDetails=response
                        this.showQueryResults.startDate=this.startDate
                        this.showQueryResults.endDate=this.endDate
                        this.startDate=null
                        this.endDate=null


                    },
                    err => {
                        this.resultDetails=[]
                        alert("there is a issue in selection of query please check")
                    }
                );
        }



        else   if(this.selectCompany!=="" && this.selectedVehicleType!=="" && this.selectedCCType!==""  ){
            this.reportServiceService.getReportByCompanyNameAndVehicleTypeAndCCType(this.selectCompany,this.selectedVehicleType,this.selectedCCType) .subscribe(
                response => {
                    console.log("ccListResult00000000ccListResultccListResultccListResult0000000000")
                    this.reportHeader=Object.keys(response[0])
                    this.resultDetails=response
                    this.showQueryResults.selectCompany=this.selectCompany
                    this.showQueryResults.selectedVehicleType=this.selectedVehicleType
                    this.selectCompany=""
                    this.selectedVehicleType=""


                },
                err => {
                    console.error(err);
                    this.resultDetails=[]
                    alert("there is a issue in selection of query please check")
                }
            );
        }
        else   if(this.selectCompany!=="" && this.selectedCCType!==""  ){
            this.reportServiceService.getReportByCompanyName(this.selectedCCType,this.selectCompany)
                .subscribe(
                    response => {
                        console.log(response[0])

                        this.reportHeader=Object.keys(response[0])
                        this.resultDetails=response
                        this.showQueryResults.selectCompany=this.selectCompany
                        this.showQueryResults.selectedVehicleType=this.selectedVehicleType
                        this.selectCompany=""
                        this.selectedCCType=""

                    },
                    err => {
                        console.error(err);
                        this.resultDetails=[]
                        alert("there is a issue in selection of query please check")
                    }
                );

        }
        else  if(this.selectCompany!=="" && this.selectedVehicleType!==""  ){
            this.reportServiceService.getReportByCompanyNameAndVehicleType(this.selectCompany,this.selectedVehicleType)
                .subscribe(
                    response => {
                        console.log("ccListResult00000000ccListResultccListResultccListResult0000000000")
                        console.log(response[0])
                        this.reportHeader=Object.keys(response[0])
                        this.resultDetails=response
                        this.showQueryResults.selectCompany=this.selectCompany
                        this.showQueryResults.selectedVehicleType=this.selectedVehicleType
                        this.selectCompany=""
                        this.selectedVehicleType=""

                    },
                    err => {
                        console.error(err);
                        this.resultDetails=[]
                        alert("there is a issue in selection of query please check")
                    }
                );

        }



    }

    resetFrom(){
        this.selectedVehicleType=""
        this.selectCompany=""
        this.selectedCCType=""
    }

    downLoadPdf(){
        this.excelService.exportAsExcelFile(this.resultDetails, 'TRANSACTIONREPORT');
    }

   /* columns = [];
    rows = [];
    fromReportData(){
        if(this.resultDetails.length>0){
            let objectKeys=Object.keys(this.resultDetails[0])
            for(let u=0;u<objectKeys.length;u++){
                let titleObj={}
                titleObj['title']=objectKeys[u].toUpperCase()
                titleObj['dataKey']=objectKeys[u]
                this.columns.push(titleObj)
            }
            console.log("8888888this.columns")
            console.log(this.columns)
            for(let j=0;j< this.resultDetails.length;j++){
                console.log(this.resultDetails[j])
                this.rows.push(this.resultDetails[j])
            }
        }

    }*/
}



