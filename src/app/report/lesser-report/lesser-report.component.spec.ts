import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LesserReportComponent } from './lesser-report.component';

describe('LesserReportComponent', () => {
  let component: LesserReportComponent;
  let fixture: ComponentFixture<LesserReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesserReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LesserReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
