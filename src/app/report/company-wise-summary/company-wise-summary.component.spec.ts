import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyWiseSummaryComponent } from './company-wise-summary.component';

describe('CompanyWiseSummaryComponent', () => {
  let component: CompanyWiseSummaryComponent;
  let fixture: ComponentFixture<CompanyWiseSummaryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyWiseSummaryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyWiseSummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
