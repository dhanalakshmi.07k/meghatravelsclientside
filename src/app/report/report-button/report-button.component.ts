import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-report-button',
  templateUrl: './report-button.component.html',
  styleUrls: ['./report-button.component.scss']
})
export class ReportButtonComponent implements OnInit {

    constructor(public router:Router) { }

    ngOnInit() {
    }

    navigatecommonReport() {
        this.router.navigate(['report']);
    }
    navigateCompnayWise() {
        this.router.navigate(['companyWiseReport']);
    }
    navigateOnDemand() {
        this.router.navigate(['onDemandReport']);
    }


}
