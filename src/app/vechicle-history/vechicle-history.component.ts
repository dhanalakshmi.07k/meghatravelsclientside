import { Component, OnInit } from '@angular/core';
import {LessorTypeServiceService} from "../../services/lessor-type-service.service";
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {FormService} from "../../services/form.service";
import {VechicleHistoryService} from "../../services/vechicle-history.service";
import {CorporateContractTypeService} from "../../services/corporate-contract-type.service";

@Component({
  selector: 'app-vechicle-history',
  templateUrl: './vechicle-history.component.html',
  styleUrls: ['./vechicle-history.component.scss']
})
export class VechicleHistoryComponent implements OnInit {
    public mainAsset: any;
    public assetConfigParameterType: any;
    public assetId: any;
    public assetFromConfig: any;
    public assetConfigName: any;
    public pagination: any;
    public skip: number;
    public limit: number;
    public allVechicleTypeHistory: any = [];

  constructor(public applicationParameteConfigService: ApplicationParameteConfigService,
              public formService: FormService,public vechicleHistoryService:VechicleHistoryService) {
      this.pagination = {
          'currentPage': 1,
          'totalPageCount': 0,
          'recordsPerPage': 10
      };
  }

  ngOnInit() {
      this.getVechicleHistoryBasedOnrange(0, 10);
      this.getAssetTotalCountForPagination();
  }



    getVechicleHistoryBasedOnrange(skip, limit) {
        this.vechicleHistoryService.getAllVechicleHistory(skip, limit)
            .subscribe(allAssets => {
                console.log(allAssets)
                this.allVechicleTypeHistory= allAssets;
            });
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getVechicleHistoryBasedOnrange(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getVechicleHistoryBasedOnrange(this.skip, this.limit);
        }
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("vechicleHistoryType")
            .subscribe((data: any) => {
                console.log("confog datae09000934-02943-0")
                console.log(data)
                this.mainAsset = false;
                this.assetConfigName = "vechicleHistoryType";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }


    getEditConfiguration(assetDetails) {
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("vechicleHistoryType")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "vechicleHistoryType"
                // console.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }

    getAssetTotalCountForPagination() {
        this.vechicleHistoryService.getVechicleHistoryCount()
            .subscribe((historyCount: any) => {
                this.pagination.totalPageCount = historyCount.count;
            });
    }

    deleteFuelTypeConfiguration(assetFuelDetails) {
        console.log("deleteLesserTypeByMongodbId ")
        console.log(assetFuelDetails)
        console.log(assetFuelDetails._id)
        this.vechicleHistoryService.deleteVechicleHistoryByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getVechicleHistoryBasedOnrange(0, 10)
            });
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }


    savedAssetCardToAssetList(savedAssetValue) {
        this.getVechicleHistoryBasedOnrange(0, 10)
    }
    updateAssetCardToAssetList(editAssetValue) {
        this.getVechicleHistoryBasedOnrange(0, 10)
    }


}
