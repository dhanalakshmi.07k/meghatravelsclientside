import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LsserShiftTypeAssoicationComponent } from './lsser-shift-type-assoication.component';

describe('LsserShiftTypeAssoicationComponent', () => {
  let component: LsserShiftTypeAssoicationComponent;
  let fixture: ComponentFixture<LsserShiftTypeAssoicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LsserShiftTypeAssoicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LsserShiftTypeAssoicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
