import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LesserCommonTypeAssociationComponent } from './lesser-common-type-association.component';

describe('LesserCommonTypeAssociationComponent', () => {
  let component: LesserCommonTypeAssociationComponent;
  let fixture: ComponentFixture<LesserCommonTypeAssociationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesserCommonTypeAssociationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LesserCommonTypeAssociationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
