import {Component, OnInit} from '@angular/core';
import {ApplicationParameteConfigService} from "../../services/application-paramete-config.service";
import {FormService} from "../../services/form.service";
import {FuelServiceService} from "../../services/fuel-service.service";

@Component({
    selector: 'app-fuel-type',

    templateUrl: './fuel-type.component.html',
    styleUrls: ['./fuel-type.component.scss']
})
export class FuelTypeComponent implements OnInit {

    assetFromConfig: any;
    assetConfigParameterType: any;
    mainAsset: any;
    assetId: any;
    assetConfigName: any;
    public skip: number;
    public limit: number;
    public allAssets: any = [];
    public pagination: any;

    constructor(public applicationParameteConfigService: ApplicationParameteConfigService, public formService: FormService, public fuelServiceService: FuelServiceService) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 12
        };
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getFuelTypeBasedOnrange(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getFuelTypeBasedOnrange(this.skip, this.limit);
        }
    }

    getParameterConfigDetails() {
        this.applicationParameteConfigService.getApplicationParameterConfig("FuelType")
            .subscribe((data: any) => {
                console.log("confog datae09000934-02943-0")
                console.log(data)
                this.mainAsset = false;
                this.assetConfigName = "fuelType";
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }

    deleteFuelTypeConfiguration(assetFuelDetails) {
        console.log("confog datae09000934-02943-0")
        console.log(assetFuelDetails._id)
        this.fuelServiceService.deleteFuelTypeByMongodbId(assetFuelDetails._id)
            .subscribe((data: any) => {
                this.getFuelTypeBasedOnrange(0, 10)
            });
    }
    deleteTransactionData:any
    intermerdiateDeleteTranasaction(transactionData){
        this.deleteTransactionData=transactionData
    }


    getEditConfiguration(assetDetails) {
        console.log("confog datae09000934-02943-0")
        console.log(assetDetails._id)
        this.assetId = assetDetails._id
        this.applicationParameteConfigService.getApplicationParameterConfig("FuelType")
            .subscribe((data: any) => {
                this.mainAsset = false;
                console.log(this.formService.formatEditAssetConfig(data.configuration, assetDetails))
                this.assetConfigParameterType = "FuelType"
                // console.log(data)
                this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetDetails)

            });
    }


    savedAssetCardToAssetList(savedAssetValue) {
        console.log("allAssetsallAssets00000000savedAssetValue00000000000")
        console.log(savedAssetValue)
        this.getFuelTypeBasedOnrange(0, 10)
    }

    updateAssetCardToAssetList(editAssetValue) {
        console.log("allAssetsallAssets00000000savedAssetValue00000000000")
        console.log(editAssetValue)
        this.getFuelTypeBasedOnrange(0, 10)
    }

    ngOnInit() {
        this.getFuelTypeBasedOnrange(0, 10)
        this.getAssetTotalCountForPagination()
    }

    getAssetTotalCountForPagination() {
        this.fuelServiceService.getFuelTypeCount()
            .subscribe((fuelCount: any) => {
                console.log("allAssetsallAssets00000000savedAssetValue00000000000")
                console.log(fuelCount.count)
                this.pagination.totalPageCount = fuelCount.count;
            });
    }


    getFuelTypeBasedOnrange(skip, limit) {
        this.fuelServiceService.getAllFuelType(skip, limit)
            .subscribe(allAssets => {
                console.log("this fuel type test listtttttttttttttttt")
                console.log(allAssets)
                this.allAssets = allAssets;
                /*     this.isShowAsset=false
                     this.allAssets = allAssets;*/
            });
    }

}
