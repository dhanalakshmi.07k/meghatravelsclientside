/**
 * Created by chandru on 13/7/18.
 */
export const menus = [

    {
        'name': 'Assets',
        'icon': 'fa-home',
        'routerLink': '/lesserAsset',
    },
    {
        'name': 'On Demand Transaction',
        'icon': 'fa-circle',
        'routerLink': '/onDemand',
    },
    {
        'name': 'Vehicle Transaction',
        'icon': 'fa-circle',
        'routerLink': '/cc2',
    },
    {
        'name': 'Shift Type',
        'icon': 'fa-circle',
        'routerLink': '/ShiftTypeCost',
    },
    {
        'name': 'Expense Type',
        'icon': 'fa-circle ',
        'routerLink': '/ExpenseType',
    },
    {
        'name': 'Service Type',
        'icon': 'fa-circle',
        'routerLink': '/ServiceType',
    },
    {
        'name': 'Vehicle History',
        'icon': 'fa-circle',
        'routerLink': '/vehicleHistory',
    },
    {
        'name': 'Vehicle Type',
        'icon': 'fa-circle',
        'routerLink': '/vehicleType',
    },
    {
        'name': 'Fuel Type',
        'icon': 'fa-circle f',
        'routerLink': '/FuelType',
    },
    {
        'name': 'Vehicle Expense',
        'icon': 'fa-circle ',
        'routerLink': '/vehicleExpense',
    },
    {
        'name': 'Vehicle Corporate Contract',
        'icon': 'fa-circle ',
        'routerLink': '/companyCorporateContract2',
    },
    {
        'name': 'User',
        'icon': 'fa-circle ',
        'routerLink': '/userManagement',
    },
    {
        'name': 'report',
        'icon': 'fa-file-pdf',
        'routerLink': '/report',
    },
    {
        'name': 'lesser Association',
        'icon': 'fa-circle',
        'routerLink': '/lesserOnDemandAssociation',
    },
    {
        'name': 'lesser report',
        'icon': 'fa-file-pdf',
        'routerLink': '/lesserReport',
    },
       {
       'name': 'corporateContract',
       'icon': 'fa-circle ',
       'routerLink': '/companyLesserAssoicationHeader',
   },

  /*  {
        'name': 'analytics',
        'icon': 'fa fa-files-o',
        'routerLink': '/analytics',
    }*/
    /*,

    {
        'name': 'settings',
        'icon': ' fa-cogs',
        'routerLink': '/settings',
    }*/

]
