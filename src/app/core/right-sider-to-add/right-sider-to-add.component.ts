import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormService} from "../../../services/form.service";
import {AssetService} from "../../../services/asset.service";
import {CorporateContractFieldsService} from "../../../services/corporate-contract-fields.service";

declare var $: any;

@Component({
    selector: 'app-right-sider-to-add',
    templateUrl: './right-sider-to-add.component.html',
    styleUrls: ['./right-sider-to-add.component.scss']
})
export class RightSiderToAddComponent implements OnInit {
    @Input() assetSelectedType: any;
    @Input() mainStatusAsset: any;
    @Output() savedAssetCardToAssetList: EventEmitter<any> = new EventEmitter();
    public output: any
    public assetFromConfig: any

    constructor(public assetService: AssetService, public formService: FormService,public corporateContractFieldsService:CorporateContractFieldsService) {
    }


    ngOnInit() {

    }

    ngOnChanges() {
        if (this.assetSelectedType) {
            this.fetchConfigBasedOnType()
        }

    }

    fetchConfigBasedOnType() {
        this.assetService.getAssetConfig(this.assetSelectedType)
            .subscribe((data: any) => {
                this.assetFromConfig = this.formService.formatAssetConfig(data.configuration)

            });
    }


    savedAssetCard(assetValue) {
        this.savedAssetCardToAssetList.emit(assetValue);
    }


}
