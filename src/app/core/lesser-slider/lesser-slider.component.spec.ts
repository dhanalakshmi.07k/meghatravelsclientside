import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LesserSliderComponent } from './lesser-slider.component';

describe('LesserSliderComponent', () => {
  let component: LesserSliderComponent;
  let fixture: ComponentFixture<LesserSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesserSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LesserSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
