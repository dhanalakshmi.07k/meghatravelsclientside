import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChange, SimpleChanges} from '@angular/core';
import {AssetService} from "../../../services/asset.service";
import {FormService} from "../../../services/form.service";

declare var $: any;

@Component({
    selector: 'app-right-sidebar',
    templateUrl: './right-sidebar.component.html',
    styleUrls: ['./right-sidebar.component.scss']
})
export class RightSidebarComponent implements OnChanges, OnInit {
    @Input() assetType: string;
    @Input() id: any;
    @Input() assetLinkedDetails: any;
    @Output() delinkAssetDetailsToAssetList: EventEmitter<any> = new EventEmitter();
    @Output() linkAssetDetailsToAssetList: EventEmitter<any> = new EventEmitter();
    @Output() syncUpdateAssetCartEvent: EventEmitter<any> = new EventEmitter();
    public resetSearchedAsset: any;
    public assetUpdatedData: any;
    public searchForAsset: string = '';
    public allAssetTypes: any;
    public individualAssetLinkedDetailsArray: any = [];
    public output: string;
    public assetFromConfig;
    public assetId;
    public delinkAssetDetails: any;

    constructor(public assetService: AssetService, public formService: FormService) {
    }

    ngOnChanges(changes: SimpleChanges) {
        let assetTypeChange: SimpleChange = changes.assetType;
        this.modify();
        this.getIndividualAssetLinkedCount();
    }

    ngOnInit() {

        this.assetService.getAssetConfig(this.assetType)
            .subscribe((data: any) => {
                if(data&&data.configuration){
                    this.assetFromConfig = this.formService.formatAssetConfig(data.configuration);
                }
            });

        this.assetService.getAllAssetTypes()
            .subscribe(assetTypes => {
                this.allAssetTypes = assetTypes;
                console.log("this.allAssetTypes", this.allAssetTypes);
            });
    }

    modify() {
        this.assetService.getAssetConfig(this.assetType)
            .subscribe((data: any) => {
                this.assetService.getAssetDetailsByMongodbId(this.id)
                    .subscribe((assetvalues: any) => {
                        this.assetId = this.id;
                        this.assetFromConfig = this.formService.formatEditAssetConfig(data.configuration, assetvalues);
                    });

            });
    }

    updateAssetCart(dataUpdated) {
        this.assetUpdatedData = dataUpdated;
        this.syncUpdateAssetCartEvent.emit(dataUpdated);
    }

    setDataFromConfig(data) {
        this.assetService.saveAssetDetails(this.formService.formatAssetSaveDetails(data))
            .subscribe(res => {
                console.log(res);
            });
    }

    getIndividualAssetLinkedCount() {
        let individualAssetLinkedCountArray: any = [];
        let obj;
        console.log('this.assetLinkedDetails', this.assetLinkedDetails);
        for (let key in this.assetLinkedDetails) {
            let value = this.assetLinkedDetails[key];
            obj = {
                'assetType': key,
                'count': value.length,
                'assetsLinked': value
            }
            individualAssetLinkedCountArray.push(obj);
        }
        this.individualAssetLinkedDetailsArray = individualAssetLinkedCountArray;
        console.log("this.individualAssetLinkedDetailsArray", this.individualAssetLinkedDetailsArray);
    }

    assetDetailsToDelink(delinkAssetDetails) {
        //this.delinkAssetDetails = delinkAssetDetails;
        this.delinkAssetDetailsToAssetList.emit(delinkAssetDetails);
    }

    /*selectedAssetTypeToLink(assetType) {
      console.log("assetType", assetType);
      this.assetTypeForSearch = assetType;
      $('#assetListingPopup').slideDown();
    }*/


    assetDetailsToLink(linkingAssetDetails) {
        this.linkAssetDetailsToAssetList.emit(linkingAssetDetails);
    }

    resetSearchedAssets() {
        this.resetSearchedAsset = Math.random();
    }

    closeAssetPopup() {
        $('#assetListingPopup').slideUp();
    }

    openTabContent(evt, cityName) {
        let i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName('tabcontent');
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = 'none';
        }
        tablinks = document.getElementsByClassName('tablinks');
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(' active', '');
        }
        document.getElementById(cityName).style.display = 'block';
        evt.currentTarget.className += ' active';
    }


}
