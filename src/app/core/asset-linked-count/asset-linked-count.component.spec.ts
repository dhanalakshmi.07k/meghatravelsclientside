import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AssetLinkedCountComponent} from './asset-linked-count.component';

describe('AssetLinkedCountComponent', () => {
    let component: AssetLinkedCountComponent;
    let fixture: ComponentFixture<AssetLinkedCountComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AssetLinkedCountComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AssetLinkedCountComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
