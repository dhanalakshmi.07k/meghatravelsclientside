import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'app-asset-linked-count',
    templateUrl: './asset-linked-count.component.html',
    styleUrls: ['./asset-linked-count.component.scss']
})
export class AssetLinkedCountComponent implements OnInit {
    @Input() assetLinkedDetails: any;

    constructor() {
    }

    ngOnInit() {
    }

}
