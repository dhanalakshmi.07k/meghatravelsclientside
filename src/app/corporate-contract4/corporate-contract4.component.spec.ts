import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateContract4Component } from './corporate-contract4.component';

describe('CorporateContract4Component', () => {
  let component: CorporateContract4Component;
  let fixture: ComponentFixture<CorporateContract4Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateContract4Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateContract4Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
