import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetButtonComponent } from './asset-button.component';

describe('AssetButtonComponent', () => {
  let component: AssetButtonComponent;
  let fixture: ComponentFixture<AssetButtonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetButtonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssetButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
