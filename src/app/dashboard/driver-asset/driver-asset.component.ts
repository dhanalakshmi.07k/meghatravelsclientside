import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import {ExcelService} from "../../../services/excel.service";
@Component({
  selector: 'app-driver-asset',
  templateUrl: './driver-asset.component.html',
  styleUrls: ['./driver-asset.component.scss']
})
export class DriverAssetComponent implements OnInit {
    isEdit:boolean=false;
    assetType : string = "Driver";
    driverAssetList:any=[];
    assetListForReport:any=[];
    driverDetails:any={}
    driverEditDetails:any={}
    public pagination: any;
    public skip: number;
    public limit: number;
    constructor(public assetService:AssetService,private excelService:ExcelService) {
        this.pagination = {
            'currentPage': 1,
            'totalPageCount': 0,
            'recordsPerPage': 10
        };
    }

    ngOnInit() {
        this.getAssetsCount()
        this.getAllAssets(0,10)
    }
    assetdriverAdded(){
        this.getAllAssets(0,10)
    }

    getAssetsCount(){
        this.assetService.getAssetPaginationCountByAssetType( this.assetType)
            .subscribe((countData: any) => {
                console.log("count===============")
                console.log(countData)
                this.pagination.totalPageCount=countData.count
            })
    }

    getAllAssets(skip,limit){
        this.assetService.getAssetByRange( this.assetType,skip,limit )
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.driverAssetList=assetList
            })
    }




    intermerdiateDeleteTranasaction(driverDetail){
        this.isEdit=true
        this.driverDetails=driverDetail
    }
    deletedriverDetails(driverDetail){
        this.assetService.deleteAssetDetailsByMongodbId(driverDetail._id)
            .subscribe((assetList: any) => {
                // this.deleteAssetCache(driverDetail)
                this.getAllAssets(0,10)
            })
    }

    deleteAssetCache(driverDetail){
        for(let j=0;j<this.driverAssetList.length;j++){
            if(this.driverAssetList[j]._id===driverDetail['_id']){
                this.driverAssetList.splice(j,1);
            }
        }
    }
    editIntermediatedriver(driverDetail){
        this.isEdit=true
        this.driverEditDetails =driverDetail
    }

    addAsset(){
        this.isEdit=false
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        }
    }

    downLoadPdf():void {
        this.assetService.getAllAssetsForReport( this.assetType)
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.assetListForReport=assetList.reverse();
                this.excelService.exportAsExcelFile(this.assetListForReport, 'DriverAsset');
            })

    }

/*    columns = [];
    rows = [];
    fromReportData(){
        if(this.assetListForReport.length>0){
            let objectKeys=Object.keys(this.assetListForReport[0])
            for(let u=0;u<objectKeys.length;u++){
                let titleObj={}
                titleObj['title']=objectKeys[u].toUpperCase()
                titleObj['dataKey']=objectKeys[u]
                this.columns.push(titleObj)
            }
            console.log("8888888this.columns")
            console.log(this.columns)
            for(let j=0;j< this.assetListForReport.length;j++){
                console.log(this.assetListForReport[j])
                this.rows.push(this.assetListForReport[j])
            }
        }

    }


    downLoadPdf(){
        this.getAllAssetsForReport()
        let doc = new jsPDF();
        doc.autoTable(this.columns, this.rows,  {addPageContent: function(data) {
                doc.text("", 40, doc.internal.pageSize.height - 10);
            }});
        doc.save('LesserAsset.pdf')
    }

    getAllAssetsForReport(){
        this.assetService.getAllAssetsForReport( this.assetType)
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.assetListForReport=assetList.reverse();
                this.fromReportData()
            })
    }*/

}
