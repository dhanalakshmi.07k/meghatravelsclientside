import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VehicleAssetComponent } from './vehicle-asset.component';

describe('VehicleAssetComponent', () => {
  let component: VehicleAssetComponent;
  let fixture: ComponentFixture<VehicleAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VehicleAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VehicleAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
