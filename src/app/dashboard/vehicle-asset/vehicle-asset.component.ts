import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import {ExcelService} from "../../../services/excel.service";

@Component({
  selector: 'app-vehicle-asset',
  templateUrl: './vehicle-asset.component.html',
  styleUrls: ['./vehicle-asset.component.scss']
})
export class VehicleAssetComponent implements OnInit {
isEdit:boolean=false;
assetType : string = "Vehicle";
vehicleAssetList:any=[];
    assetListForReport:any=[];
vehicleDetails:any={}
vehicleEditDetails:any={}
    public pagination: any;
    public skip: number;
    public limit: number;
constructor(public assetService:AssetService,private excelService:ExcelService) {
    this.pagination = {
        'currentPage': 1,
        'totalPageCount': 0,
        'recordsPerPage': 10
    };
}

  ngOnInit() {
      this.getAssetsCount()
     this.getAllAssets(0,10)

  }
    assetvehicleAdded(){
        this.getAllAssets(0,10)
    }
    getAssetsCount(){
        this.assetService.getAssetPaginationCountByAssetType( this.assetType)
            .subscribe((countData: any) => {
                console.log("count===============")
                console.log(countData)
                this.pagination.totalPageCount=countData.count
            })
    }

    getAllAssets(skip,limit){
        this.assetService.getAssetByRange( this.assetType,skip,limit )
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.vehicleAssetList=assetList.reverse()
            })
    }

    intermerdiateDeleteTranasaction(vehicleDetail){
        this.isEdit=true
       this.vehicleDetails=vehicleDetail
    }
    deletevehicleDetails(vehicleDetail){
        this.assetService.deleteAssetDetailsByMongodbId(vehicleDetail._id)
            .subscribe((assetList: any) => {
                // this.deleteAssetCache(vehicleDetail)
                this.getAllAssets(0,10)
            })
    }

    deleteAssetCache(vehicleDetail){

        for(let j=0;j<this.vehicleAssetList.length;j++){
            if(this.vehicleAssetList[j]._id===vehicleDetail['_id']){
                this.vehicleAssetList.splice(j,1);
            }
        }
    }
    editIntermediatevehicle(vehicleDetail){
       this.isEdit=true
       this.vehicleEditDetails =vehicleDetail
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        }
    }


    addAsset(){
       this.isEdit=false
    }


    downLoadPdf():void {
        this.assetService.getAllAssetsForReport( this.assetType)
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.assetListForReport=assetList.reverse();
                this.excelService.exportAsExcelFile(this.assetListForReport, 'VehicleAsset');
            })

    }

 /*   columns = [];
    rows = [];
    fromReportData(){
        if(this.assetListForReport.length>0){
            let objectKeys=Object.keys(this.assetListForReport[0])
            for(let u=0;u<objectKeys.length;u++){
                let titleObj={}
                titleObj['title']=objectKeys[u].toUpperCase()
                titleObj['dataKey']=objectKeys[u]
                this.columns.push(titleObj)
            }
            console.log("8888888this.columns")
            console.log(this.columns)
            for(let j=0;j< this.assetListForReport.length;j++){
                console.log(this.assetListForReport[j])
                this.rows.push(this.assetListForReport[j])
            }
        }

    }


    downLoadPdf(){
        this.getAllAssetsForReport()
        let doc = new jsPDF();
        doc.autoTable(this.columns, this.rows,  {addPageContent: function(data) {
                doc.text("", 40, doc.internal.pageSize.height - 10);
            }});
        doc.save('LesserAsset.pdf')
    }

    getAllAssetsForReport(){
        this.assetService.getAllAssetsForReport( this.assetType)
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.assetListForReport=assetList.reverse();
                this.fromReportData()
            })
    }*/
}
