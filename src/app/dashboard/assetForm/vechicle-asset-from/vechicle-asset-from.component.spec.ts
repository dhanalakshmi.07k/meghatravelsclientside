import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VechicleAssetFromComponent } from './vechicle-asset-from.component';

describe('VechicleAssetFromComponent', () => {
  let component: VechicleAssetFromComponent;
  let fixture: ComponentFixture<VechicleAssetFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VechicleAssetFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VechicleAssetFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
