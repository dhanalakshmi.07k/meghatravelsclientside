import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAssetFromComponent } from './company-asset-from.component';

describe('CompanyAssetFromComponent', () => {
  let component: CompanyAssetFromComponent;
  let fixture: ComponentFixture<CompanyAssetFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyAssetFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAssetFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
