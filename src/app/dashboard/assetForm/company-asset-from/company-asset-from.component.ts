import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import {FormGroup} from "@angular/forms";
import {AssetService} from "../../../../services/asset.service";
import {AssetCommunicationService} from "../../../../services/asset-communication.service";

@Component({
  selector: 'app-company-asset-from',
  templateUrl: './company-asset-from.component.html',
  styleUrls: ['./company-asset-from.component.scss']
})
export class CompanyAssetFromComponent implements OnInit {
    @Output() assetAdded: EventEmitter<any> = new EventEmitter();
    @Output() savedCompanyAssetCard: EventEmitter<any> = new EventEmitter();
    @Input() companyEditId: any;
    @Input() assetId: any;
    @Input() isEditMode: any;

    companyAssetDetails:any={
        state:"Karnataka"
    }
    isEdit:any = false;
  constructor(private assetService:AssetService,
              private assetCommunicationService:AssetCommunicationService){ }

  ngOnInit() {
      this.companyAssetDetails={
          state:"Karnataka"
      }
  }

    submitConfigDetails(){
        this.companyAssetDetails.assetType = "Company";
        this.assetService.saveAssetDetails(this.companyAssetDetails)
            .subscribe((savedAssetValue: any) => {
                this.assetAdded.emit(savedAssetValue);
                this.companyAssetDetails={
                    state:"Karnataka"
                }
            })
    }

    updateConfigDetails(){
        console.log("************this.companyAssetDetails**************")
        console.log(this.companyAssetDetails)
        this.assetService.updateAssetsById(this.companyAssetDetails._id,this.companyAssetDetails)
            .subscribe((updatedDetails: any) => {
                this.assetAdded.emit(updatedDetails);
            })
    }


    ngOnChanges() {
        if(!this.isEditMode){
            this.companyAssetDetails={
                state:"Karnataka"
            }
        }
        else{
            this.assetService.getAssetDetailsByMongodbId(this.companyEditId)
                .subscribe((assetSpeicificDetails: any) => {
                    console.log("done ------assetSpeicificDetails---")
                    console.log(assetSpeicificDetails)
                    this.companyAssetDetails=assetSpeicificDetails;
                })
        }

    }

    deleteAssetById(){
        this.assetService.deleteAssetDetailsByMongodbId(this.assetId)
            .subscribe((deleteAssetDetails: any) => {
                console.log("done ------assetSpeicificDetails---")
                console.log(deleteAssetDetails)
                this.companyAssetDetails=deleteAssetDetails;
                this.assetCommunicationService.setAssetRefresh(deleteAssetDetails)
            })
    }




}
