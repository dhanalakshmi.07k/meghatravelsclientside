import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AssetService} from "../../../../services/asset.service";
import {AssetCommunicationService} from "../../../../services/asset-communication.service";

@Component({
  selector: 'app-lesser-asset-from',
  templateUrl: './lesser-asset-from.component.html',
  styleUrls: ['./lesser-asset-from.component.scss']
})
export class LesserAssetFromComponent implements OnInit {
    @Input() assetId: any;
    @Input() isEditMode: any;
    @Input() lesserEditId: any;
    @Output() assetAdded: EventEmitter<any> = new EventEmitter();

    lesserAssetDetails:any={
        status:"Active",
        state:"Karnataka"
    }
  constructor( private assetService:AssetService,
               private assetCommunicationService:AssetCommunicationService) { }

  ngOnInit() {
        this.lesserAssetDetails={
          status:"Active",
          state:"Karnataka"
      }
  }

    submitConfigDetails() {
        this.lesserAssetDetails.assetType = "Lesser";
        this.assetService.getAssetLesserByLesserCode( this.lesserAssetDetails['LesserCode'] )
            .subscribe((lesserDetails: any) => {
                if(lesserDetails.length ===0){
                    this.assetService.saveAssetDetails( this.lesserAssetDetails )
                        .subscribe((savedAssetValue: any) => {
                            this.lesserAssetDetails={
                                status:"Active",
                                state:"Karnataka"
                            }
                            this.assetAdded.emit(savedAssetValue);
                        })
                }
                else{
                    alert("lesser is alreday configured for lesser code "+ this.lesserAssetDetails['LesserCode']);
                }
            })


    }

    ngOnChanges() {
        console.log("************this.isEdit**************")
        console.log(this.isEditMode)
        if(!this.isEditMode){
            this.lesserAssetDetails={
                status:"Active",
                state:"Karnataka"
            }
        }
        else{
            this.assetService.getAssetDetailsByMongodbId(this.lesserEditId)
                .subscribe((assetSpeicificDetails: any) => {
                    console.log("done ------assetSpeicificDetails---")
                    console.log(assetSpeicificDetails)
                    this.lesserAssetDetails=assetSpeicificDetails;
                })
        }

    }


    updateConfigDetails(){
        console.log("************this.companyAssetDetails**************")
        console.log(this.lesserAssetDetails)
        this.assetService.getAssetLesserByLesserCode( this.lesserAssetDetails['LesserCode'] )
            .subscribe((lesserDetails: any) => {
                if(lesserDetails.length ===0){
                    this.assetService.updateAssetsById(this.lesserAssetDetails._id,this.lesserAssetDetails)
                        .subscribe((updatedDetails: any) => {
                            this.assetAdded.emit(updatedDetails);
                        })
                }
                else{
                    alert("lesser is alreday configured for lesser code "+ this.lesserAssetDetails['LesserCode']);
                }
            })

    }

    deleteAssetById(){
        this.assetService.deleteAssetDetailsByMongodbId(this.assetId)
            .subscribe((deleteAssetDetails: any) => {
                console.log("done ------assetSpeicificDetails---")
                console.log(deleteAssetDetails)
                this.assetCommunicationService.setAssetRefresh(deleteAssetDetails)

            })
    }


}
