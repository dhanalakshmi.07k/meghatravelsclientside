import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LesserAssetComponent } from './lesser-asset.component';

describe('LesserAssetComponent', () => {
  let component: LesserAssetComponent;
  let fixture: ComponentFixture<LesserAssetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LesserAssetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LesserAssetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
