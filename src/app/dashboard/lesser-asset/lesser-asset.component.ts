import { Component, OnInit } from '@angular/core';
import {AssetService} from "../../../services/asset.service";
import * as jsPDF from 'jspdf';
import 'jspdf-autotable';
import {ExcelService} from "../../../services/excel.service";

@Component({
  selector: 'app-lesser-asset',
  templateUrl: './lesser-asset.component.html',
  styleUrls: ['./lesser-asset.component.scss']
})
export class LesserAssetComponent implements OnInit {
    isEdit:boolean=false;
   assetType : string = "Lesser";
   lesserAssetList:any=[];
    lesserAssetListForReport:any=[];
   lesserDetails:any={}
   lesserEditDetails:any={}
    public pagination: any;
    public skip: number;
    public limit: number;
  constructor(public assetService:AssetService,private excelService:ExcelService) {
      this.pagination = {
          'currentPage': 1,
          'totalPageCount': 0,
          'recordsPerPage': 10
      };
  }

  ngOnInit() {
      this.getAssetsCount()
      this.getAllAssets(0,10)
  }
    assetLesserAdded(){
        this.getAllAssets(0,10)
    }

    getAssetsCount(){
        this.assetService.getAssetPaginationCountByAssetType( this.assetType)
            .subscribe((countData: any) => {
                console.log("count===============")
                console.log(countData)
                this.pagination.totalPageCount=countData.count
            })
    }

    getAllAssets(skip,limit){
        this.assetService.getAssetByRange( this.assetType,skip,limit )
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.lesserAssetList=assetList.reverse();
            })
    }



    intermerdiateDeleteTranasaction(lesserDetail){
        this.isEdit=true
       this.lesserDetails=lesserDetail
    }
    deleteLesserDetails(lesserDetail){
        this.assetService.deleteAssetDetailsByMongodbId(lesserDetail._id)
            .subscribe((assetList: any) => {
                // this.deleteAssetCache(lesserDetail)
                this.getAllAssets(0,10)
            })
    }

    deleteAssetCache(lesserDetail){
        for(let j=0;j<this.lesserAssetList.length;j++){
            if(this.lesserAssetList[j]._id===lesserDetail['_id']){
                setTimeout( () => this.lesserAssetList.splice(j,1), 0);
            }
        }
        console.log("888888888888888888))))))))))))))))))this.lesserAssetList")
        console.log(this.lesserAssetList)
        console.log(this.lesserAssetList.length)
    }
    editIntermediateLesser(lesserDetail){
       this.isEdit=true
       this.lesserEditDetails =lesserDetail
    }

    getDataByPageNo(page) {
        if (page == 1) {
            this.skip = page - 1;
            this.limit = page * this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        } else {
            this.skip = (page - 1) * this.pagination.recordsPerPage;
            this.limit = this.pagination.recordsPerPage;
            this.getAllAssets(this.skip, this.limit);
        }
    }

    addAsset(){
       this.isEdit=false
    }


    columns = [];
    rows = [];
    fromReportData(){
        if(this.lesserAssetListForReport.length>0){
            let objectKeys=Object.keys(this.lesserAssetListForReport[0])
            for(let u=0;u<objectKeys.length;u++){
                let titleObj={}
                titleObj['title']=objectKeys[u].toUpperCase()
                titleObj['dataKey']=objectKeys[u]
                this.columns.push(titleObj)
            }
            console.log("8888888this.columns")
            console.log(this.columns)
            for(let j=0;j< this.lesserAssetListForReport.length;j++){
                console.log(this.lesserAssetListForReport[j])
                this.rows.push(this.lesserAssetListForReport[j])
            }
        }

    }

    downLoadPdf():void {
        this.assetService.getAllAssetsForReport( this.assetType)
            .subscribe((assetList: any) => {
                console.log("count=======????????????????????===assetListassetList=====")
                console.log(assetList)
                this.lesserAssetListForReport=assetList.reverse();
                this.excelService.exportAsExcelFile(this.lesserAssetListForReport, 'LesserAsset');
            })

    }

 /*   downLoadPdf(){
        this.getAllAssetsForReport()
        let doc = new jsPDF();
        doc.autoTable(this.columns, this.rows,  {addPageContent: function(data) {
                doc.text("", 40, doc.internal.pageSize.height - 10);
            }});
        doc.save('LesserAsset.pdf')
    }
*/


}
