import { Component, OnInit } from '@angular/core';
import {CctransactionService} from "../../services/cctransaction.service";
import {AssetService} from "../../services/asset.service";
import {VechicleTypeServiceService} from "../../services/vechicle-type-service.service";
import {VehicleCorporateContractService} from "../../services/vehicle-corporate-contract.service";
import {CorporateContractFieldsService} from "../../services/corporate-contract-fields.service";
import * as _ from "lodash";

@Component({
  selector: 'app-corporate-contract3',
  templateUrl: './corporate-contract3.component.html',
  styleUrls: ['./corporate-contract3.component.scss']
})
export class CorporateContract3Component implements OnInit {
    public vechicleTransactionSelected = "";
    public selectedVehicleItems = "";
    public companySelected = "";
    public CC4List: any = [];
    companyAssetDropDown: any;
    transactionDate:any;
    corporateContractType: any = "contract type 4"
    vechicleDropdownList: any = [];
    corporateContractFields: any;
    companyTransactionSelected: any = {}
    formConfigData: any = {};
    companySelectedDetails = ""
    public vechicleSelectedVehicleType = "";
    public vechicleSelectedDetails = {};
    companyDropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'companySelectedForDropDown',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true,
        closeDropDownOnSelection: true
    }
    vechicledropdownSettings = {
        singleSelection: false,
        idField: '_id',
        textField: 'vehicleNumber',
        selectAllText: 'Select All',
        unSelectAllText: 'UnSelect All',
        itemsShowLimit: 1,
        allowSearchFilter: true
    };

    constructor(public cctransactionService: CctransactionService,
                private assetService: AssetService,
                private vechicleTypeServiceService: VechicleTypeServiceService,
                private vehicleCorporateContractService: VehicleCorporateContractService,
                private corporateContractFieldsService: CorporateContractFieldsService) {
    }

    getIncialConfig() {
        this.selectedVehicleItems = ""
        this.companySelectedDetails = ""
        this.vechicleSelectedVehicleType = ''
        this.corporateContractFields = []
        this.getAllVechicleAsset();
        this.getCCIncialConfigData();
    }

    getCCIncialConfigData(){
        this.selectedVehicleItems=""
        this.companySelected=""
        this.corporateContractFields=[];
        this.companyTransactionSelected=""
        this.vechicleTransactionSelected=""
        this.vechicleSelectedDetails=""
        this.vechicleSelectedVehicleType=""
    }



    getAllCompanyOfTypeCC4() {
        this.vehicleCorporateContractService.getCompanyByCCType(this.corporateContractType)
            .subscribe((companyList: any) => {
                console.log("conpmy list8888888888888888888888888888888")
                console.log(companyList)
                this.companyAssetDropDown = _.uniqBy(companyList, function (companyList) {
                    return companyList['companySelectedForDropDown']
                });

                /*this.companyAssetDropDown =companyList*/
            })
    }

    getAllVechicleAsset() {
        this.assetService.getAssetlistSpecificList("Vehicle")
            .subscribe((vechicleList: any) => {
                console.log("******vechicleList*************");
                console.log(vechicleList)
                this.vechicleDropdownList = vechicleList;

            })
    }

    getCCTransaction() {
        this.cctransactionService.getCCTransactionByType(this.corporateContractType)
            .subscribe(CCListDetails => {
                this.CC4List = CCListDetails
            });
    }


    onvehicleSelect(vehicleDetails) {
        console.log("8328472384718vehicleDetails92347891237413")
        console.log(vehicleDetails._id)
        this.assetService.getAssetDetailsByMongodbId(vehicleDetails._id)
            .subscribe((vehicleDetails: any) => {
                console.log("******vechicleList*************");
                console.log(vehicleDetails);
                this.vechicleSelectedDetails = vehicleDetails
                this.vechicleSelectedVehicleType = vehicleDetails.vechicleType

            })
    }


    onCompanySelect(companyDetails) {
        this.companyTransactionSelected = companyDetails
        this.vehicleCorporateContractService.getVehicleCorporateContractByMongodbId(companyDetails['_id'])
            .subscribe((companyCC: any) => {
                console.log("this.onCompanySelect-=------------+++++++++++++++++++--------")
                console.log(companyCC[0].companySelected._id)
                console.log(this.corporateContractType)
                console.log(this.vechicleSelectedVehicleType['vehicleTypeName'])
                this.vehicleCorporateContractService.getcompanyCCByMongodbIdAndCCTypeAndvehicleType(companyCC[0].companySelected._id, this.corporateContractType
                    , this.vechicleSelectedVehicleType['vehicleTypeName'])
                    .subscribe((companyCCDetails: any) => {
                        if (companyCCDetails && companyCCDetails[0]) {
                            companyCCDetails = companyCCDetails[0];
                            for (var g = 0; g < this.corporateContractFieldsService.corporateContractTypes.length; g++) {
                                if (this.corporateContractFieldsService.corporateContractTypes[g]['assetType']['typeId'] ==
                                    companyCCDetails.corporateContract) {
                                    let CCFieldsConfiguration = Object.keys(this.corporateContractFieldsService.corporateContractTypes[g].configuration);
                                    let companyCCKeys = Object.keys(companyCCDetails);
                                    let VehicleCCKeys = Object.keys(this.vechicleSelectedDetails);
                                    if (companyCCKeys) {
                                        for (var k = 0; k < companyCCKeys.length; k++) {
                                            for (let p = 0; p < CCFieldsConfiguration.length; p++) {
                                                if (companyCCKeys[k] == CCFieldsConfiguration[p]) {
                                                    this.formConfigData
                                                        [this.corporateContractFieldsService.corporateContractTypes[g].configuration[CCFieldsConfiguration[p]]['field']] =
                                                        companyCCDetails[companyCCKeys[k]]
                                                }
                                            }
                                        }

                                        this.formConfigData['vehicleType'] = this.vechicleSelectedVehicleType['vehicleTypeName'];

                                    }
                                    console.log(this.corporateContractFieldsService.corporateContractTypes[g])
                                    this.fromConfigObjectForFrom(this.corporateContractFieldsService.corporateContractTypes[g], companyCCDetails)
                                }

                            }
                        }
                        else {
                            alert("selected vehicle type  is not configured")
                        }
                    });
            })


    }

    fromConfigObjectForFrom(configDetailsObj, companyCCDetails) {
        let configObj = configDetailsObj.configuration
        let responseKeys = Object.keys(configObj);
        let formData = [];
        for (var prop of responseKeys) {
            formData.push(configObj[prop]);
        }
        if (formData) {
            this.corporateContractFields = formData;
        }


    }

    deleteCC1Trasnaction(ccDetails) {
        console.log("ccDetails0000000000000werwerwer------------")
        console.log(ccDetails)
        this.cctransactionService.deleteCCTransactionByMongodbId(ccDetails._id)
            .subscribe((data: any) => {
                this.getCCTransaction()
            })
    }


    submitTransaction() {
        var obj = {}
        obj['minKms'] = this.formConfigData['minKms']
        obj['minAmt'] = this.formConfigData['minAmt']
        obj['ratePerKm'] = this.formConfigData['ratePerKm']
        obj['getIncialConfigtotalKms'] = this.formConfigData['totalKms']
        obj['vehicleType'] = this.vechicleSelectedVehicleType
        obj['vehicleId'] = this.vechicleSelectedDetails
        obj['vehicleTransactionType']=this.formConfigData['vehicleType']
        obj['companyTransactionSelected'] = this.companyTransactionSelected
        obj['corporateContract'] = this.corporateContractType
        obj['transactionDate']=this.transactionDate
        obj['extraKms'] = this.formConfigData['minKms'] - this.formConfigData['totalKms']
        obj['totalAmt'] = (this.formConfigData['ratePerKm'] * Math.abs(obj['extraKms'])) + obj['minAmt']
        console.log("88888888888888888888888888------finalll-------");
        console.log(obj);
        this.cctransactionService.saveCCTransaction(obj)
            .subscribe((data: any) => {
                this.getCCTransaction()
            });


    }


    ngOnInit() {
        this.getAllCompanyOfTypeCC4();
        this.getCCTransaction();
    }
}
