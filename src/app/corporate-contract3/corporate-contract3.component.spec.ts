import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CorporateContract3Component } from './corporate-contract3.component';

describe('CorporateContract3Component', () => {
  let component: CorporateContract3Component;
  let fixture: ComponentFixture<CorporateContract3Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CorporateContract3Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CorporateContract3Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
