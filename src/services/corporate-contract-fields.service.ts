import { Injectable } from '@angular/core';

@Injectable()
export class CorporateContractFieldsService {



  public corporateContractTypes = [
      {
          "assetType": {
              "typeId": "contract type 1",
          },
          "configuration": {

              "RatePerTrip" : {
                  "field":"RatePerTrip",
                  "type" : "number",
                  "description" : "Enter Rate Per Trip",
                  "label":"Rate Per Trip",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "ExtradutyRate" : {
                  "field":"ExtradutyRate",
                  "type" : "number",
                  "description" : "Enter Extra Duty Rate",
                  "label":"Extra Duty Rate",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "TDS" : {
                  "field":"TDS",
                  "type" : "number",
                  "description" : "Enter TDS",
                  "label":"TDS(%)",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },

              "vehicleType" : {
                  "field":"vehicleType",
                  "type" : "number",
                  "description" : "Enter vehicle Type",
                  "label":"vehicleType",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"text",
                  "defaultValueExists":false
              },
              "numberOfTrips" : {
                  "field":"numberOfTrips",
                  "type" : "number",
                  "description" : "Enter Number Of Trips",
                  "label":"Number Of Trips",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },

              "gps" : {
                  "field":"gps",
                  "type" : "number",
                  "description" : "Enter GPS",
                  "label":"GPS",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              }

          }
      },
      {
          "assetType": {
              "typeId": "contract type 2",
          },
          "configuration": {
              "costPerkm" : {
                  "field":"costPerkm",
                  "type" : "number",
                  "description" : "Enter Cost Per km",
                  "label":"Cost Per km",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "isDriverCostField":false,
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "leasedAmount" : {
                  "field":"leasedAmount",
                  "type" : "number",
                  "description" : "Enter Leased Amount ",
                  "label":"Leased Amount",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "fixedCostSingleDriver" : {
                  "field":"fixedCostSingleDriver",
                  "type" : "number",
                  "description" : "Enter Fixed Cost Single Driver",
                  "label":"Fixed Cost Single Driver",
                  "required":false,
                  "autoComplete":false,
                  "isDriverCostField":true,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "fixedCostDoubleDriver" : {
                  "field":"fixedCostDoubleDriver",
                  "type" : "number",
                  "description" : "Enter Fixed Cost Double Driver",
                  "label":"Fixed Cost Double Driver",
                  "required":false,
                  "autoComplete":false,
                  "isDriverCostField":true,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "totalKms" : {
                  "field":"totalKms",
                  "type" : "number",
                  "description" : "Enter Total Kms",
                  "label":"Total Kms",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "halting" : {
                  "field":"halting",
                  "type" : "number",
                  "description" : "Enter Halting Cost",
                  "label":"Halting Cost",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },

              "tollFees" : {
                  "field":"tollFees",
                  "type" : "number",
                  "description" : "Enter Toll Fees",
                  "label":"Toll Fees",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
             /* "fxdAndLesCost" : {
                  "field":"fxdAndLesCost",
                  "type" : "number",
                  "description" : "Enter Fixed And Leased Cost",
                  "label":"Fixed And Leased Cost",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "isDriverCostField":false,
                  "defaultValueExists":false
              },*/

            /*  "vehicleType" : {
                  "field":"vehicleType",
                  "type" : "number",
                  "description" : "Enter vehicle Type",
                  "label":"vehicleType",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"text",
                  "defaultValueExists":false
              },*/
             /* "noOfVehicle" : {
                  "field":"noOfVehicle",
                  "type" : "number",
                  "description" : "Enter No Of Vehicle",
                  "label":" No Of Vehicle",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },*/

          }

      },
      {
          "assetType": {
              "typeId": "contract type 3",
          },
          "configuration": {

              "ratePerKm" : {
                  "field":"ratePerKm",
                  "type" : "number",
                  "description" : "Enter rate Per Km",
                  "label":"Rate Per Km",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "minKms" : {
                  "field":"minKms",
                  "type" : "number",
                  "description" : "Enter Min Kms",
                  "label":"Min Kms",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "minAmt" : {
                  "field":"minAmt",
                  "type" : "number",
                  "description" : "Enter Min Amt",
                  "label":"Min Amt",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "totalKms" : {
                  "field":"totalKms",
                  "type" : "number",
                  "description" : "Enter Total Kms",
                  "label":"Total Kms",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              }


          }

      },
      {
          "assetType": {
              "typeId": "OnDemand",
          },
          "configuration": {
              "ratePerHr" : {
                  "field":"ratePerHr",
                  "type" : "number",
                  "description" : "Enter Rate Per Hr",
                  "label":"Rate Per Hr",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":true
              },
              "ratePerKm" : {
                  "field":"ratePerKm",
                  "type" : "number",
                  "description" : "Enter Rate Per Km",
                  "label":"Rate Per Km",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":true
              },
              "slabRate4hrs40kmts" : {
                  "field":"slabRate4hrs40kmts",
                  "type" : "number",
                  "description" : "Enter slab Rate 4hrs 40kmts",
                  "label":"slab Rate 4hrs 40kmts",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":true
              },
              "slabRate8hrs80kmts" : {
                  "field":"slabRate8hrs80kmts",
                  "type" : "number",
                  "description" : "Enter slab Rate 8hrs 80kmts",
                  "label":"slab Rate 8hrs 80kmts",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":true
              },
              "TSNo" : {
                  "field":"TSNo",
                  "type" : "text",
                  "description" : "Enter TSNo",
                  "label":"TS No",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"text",
                  "defaultValueExists":false
              },
              "totalDaysPerHour" : {
                  "field":"totalDaysPerHour",
                  "type" : "number",
                  "description" : "Enter Total Days Per Hour",
                  "label":"Total Days Per Hour",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "totalKms" : {
                  "field":"totalKms",
                  "type" : "number",
                  "description" : "Enter Total Kms",
                  "label":"Total Kms",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },


           /*   "vehicleType" : {
                  "field":"vehicleType",
                  "type" : "number",
                  "description" : "Enter vehicle Type",
                  "label":"vehicleType",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"text",
                  "defaultValueExists":false
              },*/
              /*      "bookedBy" : {
                  "field":"bookedBy",
                  "type" : "text",
                  "description" : "Enter booked By",
                  "label":"booked By",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"text",
                  "defaultValueExists":false
              },*/



          }

      },

      {
          "assetType": {
              "typeId": "contract type 5"
          },
          "configuration": {
              "totalKms": {
                  "field": "totalKms",
                  "type": "number",
                  "description": "Enter Total Kms",
                  "label": "Total Kms",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "ratePerKm": {
                  "field": "ratePerKm",
                  "type": "number",
                  "description": "Enter Rate Per Km",
                  "label": "Rate Per Km",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "fixedCost": {
                  "field": "fixedCost",
                  "type": "number",
                  "description": "Enter fixed Cost",
                  "label": "fixed Cost",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              }

          }

      },
      {
          "assetType": {
              "typeId": "contract type 6"
          },
          "configuration": {
              "route": {
                  "field": "route",
                  "type": "text",
                  "description": "Enter Route",
                  "label": "Route",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "text",
                  "defaultValueExists": false
              },
              "totalKms" : {
                  "field":"totalKms",
                  "type" : "number",
                  "description" : "Enter Total Kms",
                  "label":"Total Kms",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"transcation",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "noOfDays": {
                  "field": "noOfDays",
                  "type": "text",
                  "description": "Enter Number Of Days",
                  "label": "Number Of Days",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "text",
                  "defaultValueExists": false
              },
              "minimumDays": {
                  "field": "minimumDays",
                  "type": "number",
                  "description": "Enter Minimum Days",
                  "label": "Minimum Days",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "fixedAmt" : {
                  "field":"fixedAmt",
                  "type" : "number",
                  "description" : "Enter Fixed Amount",
                  "label":"Fixed Amount",
                  "required":false,
                  "autoComplete":false,
                  "configurationType":"configuration",
                  "fieldDisplayType":"number",
                  "defaultValueExists":false
              },
              "ratePerKm": {
                  "field": "ratePerKm",
                  "type": "number",
                  "description": "Enter Rate Per Km",
                  "label": "Rate Per Km",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
                "minimumKms": {
                    "field": "minimumKms",
                    "type": "number",
                    "description": "Enter Minimum Kms",
                    "label": "Minimum Kms",
                    "required": false,
                    "autoComplete": false,
                    "configurationType": "configuration",
                    "fieldDisplayType": "number",
                    "defaultValueExists": false
                },







          }
      },
      {
          "assetType": {
              "typeId": "Shift Type"
          },
          "configuration": {
              "fixedKmsDay": {
                  "field": "fixedKmsDay",
                  "type": "number",
                  "description": "Enter Fixed Kms Day",
                  "label": "Fixed Kms Day",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "ratePerKm": {
                  "field": "ratePerKm",
                  "type": "number",
                  "description": "Enter Rate Per Km",
                  "label": "Rate Per Km",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "ratePerShift": {
                  "field": "ratePerShift",
                  "type": "number",
                  "description": "Enter Rate Per Shift",
                  "label": "Rate Per Shift",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "minimumDaysFixed": {
                  "field": "minimumDaysFixed",
                  "type": "number",
                  "description": "Enter Minimum Days Fixed",
                  "label": "Minimum Days Fixed",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "route": {
                  "field": "route",
                  "type": "text",
                  "description": "Enter Route",
                  "label": "Route",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "text",
                  "defaultValueExists": false
              },
              "kmsPerDay": {
                  "field": "kmsPerDay",
                  "type": "number",
                  "description": "Enter kms Per Day",
                  "label": "kms Per Day",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "numberOfDays":{
                  "field": "numberOfDays",
                  "type": "number",
                  "description": "Enter number Of Days",
                  "label": "kms Per Day",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },

              "splitTrip":{
                  "field": "splitTrip",
                  "type": "number",
                  "description": "Enter split Trip",
                  "label": "split Trip",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },


/*
              "scheduledKmsDay": {
                  "field": "scheduledKmsDay",
                  "type": "number",
                  "description": "Enter Scheduled Kms Day",
                  "label": "Scheduled Kms Day",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },*/

          }
      },
      {
          "assetType": {
              "typeId": "fixedDayKmContract"
          },
          "configuration": {
              "noOfKms": {
                  "field": "noOfKms",
                  "type": "number",
                  "description": "Enter No Of Kms",
                  "label": "No Of Kms",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "noOfDays": {
                  "field": "noOfDays",
                  "type": "text",
                  "description": "Enter  No of Days",
                  "label": "No of Days",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "transcation",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },

              "minDaysPerMonth": {
                  "field": "minDaysPerMonth",
                  "type": "text",
                  "description": "Enter  Min Days Per Month",
                  "label": "Min Days Per Month",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "text",
                  "defaultValueExists": false
              },
              "minKMsPerMonth": {
                  "field": "minKMsPerMonth",
                  "type": "number",
                  "description": "Enter Min KMs Per Month",
                  "label": "Min KMs Per Month",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "fixedAmount":{
                  "field": "fixedAmount",
                  "type": "number",
                  "description": "Enter Fixed Amount",
                  "label": "Fixed Amount",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },

              "ratePerKM": {
                  "field": "ratePerKM",
                  "type": "number",
                  "description": "Enter Rate Per KM",
                  "label": "Rate Per KM",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },
              "ratePerDay": {
                  "field": "ratePerDay",
                  "type": "number",
                  "description": "Enter Rate Per Day",
                  "label": "Rate Per Day",
                  "required": false,
                  "autoComplete": false,
                  "configurationType": "configuration",
                  "fieldDisplayType": "number",
                  "defaultValueExists": false
              },


          }
      },


      ]

  constructor() { }

}
