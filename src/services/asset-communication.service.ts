import { Injectable } from '@angular/core';
import {Observable, Subject} from "rxjs";

@Injectable()
export class AssetCommunicationService {
    private subject = new Subject<any>();

  constructor() { }
    setAssetRefresh(assetDetails: string) {
        this.subject.next({ assetDetail: assetDetails });
    }
    getAssetRefresh(): Observable<any> {
        return this.subject.asObservable();
    }

}
