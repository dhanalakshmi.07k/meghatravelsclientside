import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable()
export class FormService {
    expenseFromArr=[
        {
        "assetType": {
            "typeId": "SpareParts",
            "label": "SpareParts",
            theme: {
                color: {
                    primary: "#5C6BC0",
                    secondary: "#7986CB"
                }
            },
            order: 1
        },
        "configuration": {
            "sparePartName" : {
                "type" : "text",
                "field":"sparePartName",
                "description" : "Enter Spare Part Name",
                "label":"Spare Part Name",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "amount" : {
                "type" : "text",
                "field":"Amount",
                "description" : "Enter Amount",
                "label":"Amount",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            }
            }

    },
        {
            "assetType": {
                "typeId": "Accident",
                "label": "Accident",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "amount" : {
                    "type" : "text",
                    "field":"Amount",
                    "description" : "Enter Amount",
                    "label":"Amount",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "serviceCenter" : {
                    "type" : "text",
                    "field":"Service Center",
                    "description" : "Enter Service Center",
                    "label":"Service Center",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },

            "fixedDate" : {
                "type" : "date",
                "field":"fixedDate",
                "description" : "Enter Fixed Date",
                "label":"Fixed Date",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"date"
            },
                "accidentDescription" : {
                    "type" : "text",
                    "field":"accidentDescription",
                    "description" : "Enter Accident Description",
                    "label":"Accident Description",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"textarea"
                },
            }

        },
        {
            "assetType": {
                "typeId": "TyreChange",
                "label": "TyreChange",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "manufacturerNumber" : {
                    "type" : "text",
                    "field":"manufacturerNumber",
                    "description" : "Enter Manufacturer Number",
                    "label":"Manufacturer Number",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "tyreType" : {
                    "type" : "text",
                    "field":"tyreType",
                    "description" : "Enter  Tyre Type",
                    "label":" Tyre Type",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },

                "size" : {
                    "type" : "text",
                    "field":"size",
                    "description" : "Enter Size",
                    "label":"Size",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "fixedDate" : {
                    "type" : "date",
                    "field":"fixedDate",
                    "description" : "Enter Fixed Date",
                    "label":"Fixed Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "removedDate" : {
                    "type" : "date",
                    "field":"removedDate",
                    "description" : "Enter Removed Date",
                    "label":"Removed Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "amount" : {
                    "type" : "text",
                    "field":"Amount",
                    "description" : "Enter Amount",
                    "label":"Amount",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                }
            }

        },
        {
            "assetType": {
                "typeId": "Battery",
                "label": "Battery",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "manufacturerNumber" : {
                    "type" : "text",
                    "field":"manufacturerNumber",
                    "description" : "Enter Manufacturer Number",
                    "label":"Manufacturer Number",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "platesNumber" : {
                    "type" : "text",
                    "field":"platesNumber",
                    "description" : "Enter  Plates Number",
                    "label":" Plates Number",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "fixedDate" : {
                    "type" : "date",
                    "field":"fixedDate",
                    "description" : "Enter Fixed Date",
                    "label":"Fixed Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "removedDate" : {
                    "type" : "date",
                    "field":"removedDate",
                    "description" : "Enter Removed Date",
                    "label":"Removed Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "amount" : {
                    "type" : "text",
                    "field":"Amount",
                    "description" : "Enter Amount",
                    "label":"Amount",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                }
            }

        },
        {
            "assetType": {
                "typeId": "FitnessCertificate",
                "label": "FitnessCertificate",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "registrationDate" : {
                    "type" : "date",
                    "field":"registrationDate",
                    "description" : "Enter  Registration Date",
                    "label":"Registration Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "expirydate" : {
                    "type" : "date",
                    "field":"expirydate",
                    "description" : "Enter Expiry date",
                    "label":"Expiry date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                }
            }

        },
        {
            "assetType": {
                "typeId": "CurrentReading",
                "label": "CurrentReading",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "distanceTraveledActual": {
                    "type": "text",
                    "field": "distanceTraveledActual",
                    "description": "Enter  Distance Traveled Actual (KM Reading)",
                    "label": "Distance Traveled Actual",
                    "required": true,
                    "autoComplete": false,
                    "fieldDisplayType": "text"
                },
                "distanceTraveledEstimated": {
                    "type": "text",
                    "field": "distanceTraveledEstimated",
                    "description": "Distance Traveled Estimated",
                    "label": "Distance Traveled Estimated",
                    "required": true,
                    "autoComplete": false,
                    "fieldDisplayType": "text"
                },
                "grease": {
                    "type": "text",
                    "field": "grease",
                    "description": "Enter Grease",
                    "label": "Grease",
                    "required": true,
                    "autoComplete": false,
                    "fieldDisplayType": "text"
                },
                "dieselPetrolReading": {
                    "type": "text",
                    "field": " dieselPetrolReading",
                    "description": "Enter  Diesel/Petrol Reading",
                    "label": "Diesel/Petrol Reading",
                    "required": true,
                    "autoComplete": false,
                    "fieldDisplayType": "text"
                },
                "engineLubeOil": {
                    "type": "text",
                    "field": "engineLubeOil",
                    "description": "Enter  Engine/lube Oil",
                    "label": "Engine/lube Oil",
                    "required": true,
                    "autoComplete": false,
                    "fieldDisplayType": "text"
                },
                "kMPerLiter": {
                    "type": "text",
                    "field": "kMPerLiter",
                    "description": "Enter   KM per Liter",
                    "label": "KM per Liter",
                    "required": true,
                    "autoComplete": false,
                    "fieldDisplayType": "text"
                },
                "registrationDate" : {
                    "type" : "date",
                    "field":"registrationDate",
                    "description" : "Enter  Registration Date",
                    "label":"Registration Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "expirydate" : {
                    "type" : "date",
                    "field":"expirydate",
                    "description" : "Enter Expiry date",
                    "label":"Expiry date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                }
            }
        },{
            "assetType": {
                "typeId": "InsuranceDetails",
                "label": "InsuranceDetails",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "insuranceCompany" : {
                    "type" : "text",
                    "field":"Insurance Company",
                    "description" : "Enter  Insurance Company",
                    "label":"insuranceCompany",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "insuranceNumber" : {
                    "type" : "text",
                    "field":"insuranceNumber",
                    "description" : "Enter Insurance Number",
                    "label":"Insurance Number",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "insuredDate" : {
                    "type" : "date",
                    "field":"insuredDate",
                    "description" : "Enter Insured Date",
                    "label":"Insured Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "renewalDate" : {
                    "type" : "date",
                    "field":"renewalDate",
                    "description" : "Enter Renewal Date",
                    "label":"Renewal Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "amount" : {
                    "type" : "text",
                    "field":"Amount",
                    "description" : "Enter Amount",
                    "label":"Amount",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "registrationDate" : {
                    "type" : "date",
                    "field":"registrationDate",
                    "description" : "Enter  Registration Date",
                    "label":"Registration Date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                },
                "expirydate" : {
                    "type" : "date",
                    "field":"expirydate",
                    "description" : "Enter Expiry date",
                    "label":"Expiry date",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"date"
                }
            }
        },
        {
            "assetType": {
                "typeId": "Repair",
                "label": "Repair",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {

            }

        },
        {
            "assetType": {
                "typeId": "EngineRepair",
                "label": "EngineRepair",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "serviceCenter" : {
                    "type" : "text",
                    "field":"serviceCenter",
                    "description" : "Enter  Service Center",
                    "label":"Service Center",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "intime" : {
                    "type" : "text",
                    "field":"intime",
                    "description" : "Enter  In time",
                    "label":"In time",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "outTime" : {
                    "type" : "text",
                    "field":"outTime",
                    "description" : "Enter Out Time",
                    "label":"Out Time",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },

                "serviceCenterJobId" : {
                    "type" : "text",
                    "field":"serviceCenterJobId",
                    "description" : "Enter Service Center Job Id",
                    "label":"Service Center Job Id",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "amount" : {
                    "type" : "text",
                    "field":"Amount",
                    "description" : "Enter Amount",
                    "label":"Amount",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "repairDescription" : {
                    "type" : "textarea",
                    "field":"repairDescription",
                    "description" : "Enter Repair Description",
                    "label":"Repair Description",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"textarea"
                }
            }

        },

        {
            "assetType": {
                "typeId": "ChassisRepair",
                "label": "ChassisRepair",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "serviceCenter" : {
                    "type" : "text",
                    "field":"serviceCenter",
                    "description" : "Enter  Service Center",
                    "label":"Service Center",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "intime" : {
                    "type" : "text",
                    "field":"intime",
                    "description" : "Enter  In time",
                    "label":"In time",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "outTime" : {
                    "type" : "text",
                    "field":"outTime",
                    "description" : "Enter Out Time",
                    "label":"Out Time",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },

                "serviceCenterJobId" : {
                    "type" : "text",
                    "field":"serviceCenterJobId",
                    "description" : "Enter Service Center Job Id",
                    "label":"Service Center Job Id",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "amount" : {
                    "type" : "text",
                    "field":"Amount",
                    "description" : "Enter Amount",
                    "label":"Amount",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "repairDescription" : {
                    "type" : "textarea",
                    "field":"repairDescription",
                    "description" : "Enter Repair Description",
                    "label":"Repair Description",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"textarea"
                }
            }

        },
        {
            "assetType": {
                "typeId": "BodyRepair",
                "label": "BodyRepair",
                theme: {
                    color: {
                        primary: "#5C6BC0",
                        secondary: "#7986CB"
                    }
                },
                order: 1
            },
            "configuration": {
                "serviceCenter" : {
                    "type" : "text",
                    "field":"serviceCenter",
                    "description" : "Enter  Service Center",
                    "label":"Service Center",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "intime" : {
                    "type" : "text",
                    "field":"intime",
                    "description" : "Enter  In time",
                    "label":"In time",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "outTime" : {
                    "type" : "text",
                    "field":"outTime",
                    "description" : "Enter Out Time",
                    "label":"Out Time",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },

                "serviceCenterJobId" : {
                    "type" : "text",
                    "field":"serviceCenterJobId",
                    "description" : "Enter Service Center Job Id",
                    "label":"Service Center Job Id",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "amount" : {
                    "type" : "text",
                    "field":"Amount",
                    "description" : "Enter Amount",
                    "label":"Amount",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"text"
                },
                "repairDescription" : {
                    "type" : "textarea",
                    "field":"repairDescription",
                    "description" : "Enter Repair Description",
                    "label":"Repair Description",
                    "required":true,
                    "autoComplete":false,
                    "fieldDisplayType":"textarea"
                }
            }

        },

    ]

    constructor(private http: HttpClient) {
    }

    getExpenseTypefromConfig(){
        return this.expenseFromArr
    }

    formatAssetConfig(assetConfig) {
        let arr = []
        for (let key in assetConfig) {
            if (assetConfig.hasOwnProperty(key)) {
                arr.push(assetConfig[key])
            }
        }
        return arr;
    }

    formatEditAssetConfig(assetConfig, assetDetails) {
        let arr = []
        for (let key in assetConfig) {
            if (assetConfig.hasOwnProperty(key)) {
                assetConfig[key].fieldValue = assetDetails[key];
                arr.push(assetConfig[key])
            }
        }
        return arr;
    }


    formatEditAssetDetails(assetDetails) {
        var obj = {}
        for (let key in assetDetails) {
            if (assetDetails.hasOwnProperty(key)) {
                obj[assetDetails[key].field] = assetDetails[key].fieldValue
            }
        }
        return obj
    }

    formatAssetSaveDetails(assetDetails) {
        let assetModifiedObj = {};
        for (let key in assetDetails) {
           /* console.log("000000000if0000000000assetModifiedObj000000000000----")
            console.log(assetDetails.hasOwnProperty(key) )
            console.log( key  )
            console.log( typeof assetDetails[key]  )*/
            if (assetDetails.hasOwnProperty(key) && (typeof  assetDetails[key] === 'string'||typeof  assetDetails[key] === 'number')){
                /*console.log("000000000if0000000000assetModifiedObj000000000000----")
                console.log( assetModifiedObj[key])
                console.log( assetDetails[key])*/
                assetModifiedObj[key] = assetDetails[key]
            }
            else if (typeof  assetDetails[key] == 'object' && !assetDetails[key].hasOwnProperty("type")) {
               /* console.log("0000000000000000000000000000000----")
                console.log(assetDetails[key])*/

                const obj = assetDetails[key]
                let arr = [];
                for (let p in obj)
                    arr.push(obj[p]);
                const str = arr.join('-');
              /*  console.log("0000000000000000000")
                console.log(str)*/
                assetModifiedObj['modifieDate'] = str
                assetModifiedObj[key] = assetDetails[key]
            }
        }


        return assetModifiedObj;
    }
}
