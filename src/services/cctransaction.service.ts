import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class CctransactionService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveCCTransaction(CCTransactionData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'CCTransaction', CCTransactionData);
    }

    getCCTransactionCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'CCTransaction/count');
    }

    getCCTransactionCost(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'CCTransaction' + '/' + skip + '/' + limit);
    }

    getCCTransactionByType(ccType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'CCTypeTransaction' + '/' + ccType);
    }

    getCCTransactionByTypeByskipType(ccType,skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'CCTypeTransactionByLmit' + '/' + ccType+ '/' + skip + '/' + limit);
    }

    getCCTransactionByIdByMongodbId(CCMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'CCTransactionById/' + CCMongoId);
    }

    updateCCTransactionById(CCTransactionId, CCTransactionObj) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editCCTransactionModel/' + CCTransactionId, CCTransactionObj);
    }


    deleteCCTransactionByMongodbId(CCTransactionMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'CCTransactionByiD/' + CCTransactionMongoId);
    }






}
