import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class ShiftTypeServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveShiftTypeCostDetails(shiftTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'shiftDetails', shiftTypeData);
    }

    getAllShiftTypeCost(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allShiftDetails' + '/' + skip + '/' + limit);
    }

    getShiftTypeCostByMongodbId(shiftMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'shiftBymongoId/' + shiftMongoId);
    }

    updateShiftTypeCostById(shiftId, shiftTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'editShiftBymongoId/' + shiftId, shiftTypeData);
    }


    deleteShiftTypeCostByMongodbId(shiftMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'shiftBymongoId/' + shiftMongoId);
    }

    getShiftTypeCostCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'shiftType/count');
    }

    getShiftTypeCostDropDown() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allShiftDetailDropDown');
    }
}
