import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class VechicleTransactionService {

  constructor(private http: HttpClient, public configService: ConfigService) { }

    saveVechicleTransaction(vechicleTransactionData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'saveDailyVechicleTransaction', vechicleTransactionData);
    }

    getAllVechicleTransaction(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allVechicleTransaction' + '/' + skip + '/' + limit);
    }

    getVechicleTransactionByMongodbId(transactionID) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'VechicleTransactionBymongoId/' + transactionID);
    }

    updateVechicleTransactionById(vechicleTransactionMongoId, details) {
        console.log("----------------responseee")
        console.log(vechicleTransactionMongoId)
        console.log(details)
        return this.http.post(this.configService.appConfig.appBaseUrl + 'VechicleTransactionlDetails/' + vechicleTransactionMongoId, details);
    }


    deleteVechicleTransactionByMongodbId(vechicleMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'VechicleTransactionBymongoId/' + vechicleMongoId);
    }

    getVechicleTransactionCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allVechicleTransaction/count');
    }

}
