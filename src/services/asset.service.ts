/**
 * Created by chandru on 29/6/18.
 */
import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ConfigService} from "./config.service";

@Injectable()
export class AssetService {
    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    getAssetConfig(assetName) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetConfig?type=' + assetName);
    }

    getAssetPaginationCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'getAllAssetCount');
    }

    getAssetsBasedOnRange(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByLimit?skip=' + skip + '&limit=' + limit);
    }

    getAssetPaginationCountByAssetType(assetType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/countByAssetType/'+assetType);
    }


    getCountByTypes(assetTypes) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/countByType?type=' + assetTypes);
    }
    getAssetByRange(assetTypes,skip,limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByAssetTypeByRange/'+assetTypes+'/'+skip+'/'+limit);
    }


    getAllByType(assetTypes, skip, limit) {
        let url = this.configService.appConfig.appBaseUrl + 'assetsByLimit?skip=' + skip + '&limit=' + limit;
        if (assetTypes) {
            url = url + '&type=' + assetTypes;
        }
        return this.http.get(url);
    }

    getAllAssetTypes() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'types');
    }

    getAssetCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/countByType');
    }


    saveAssetDetails(assetData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'assetDetails', assetData);
    }


    getAllAssets() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'asset');
    }


    getAssetDetailsByMongodbId(assetId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'asset/' + assetId);
    }

    getAssetDetailsByVehicleId(vehicleId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'vechileAssetDetailsByVehicelId/' + vehicleId);
    }

    updateAssetsById(assetId, assetDetails) {
        return this.http.put(this.configService.appConfig.appBaseUrl + 'asset/' + assetId, assetDetails);
    }

    saveAssets(assetDetails) {
        return this.http.put(this.configService.appConfig.appBaseUrl + 'asset/', assetDetails);
    }

    searchForAssetFromassetType(searchForAsset, assetTypeForSearch) {
        let queryUrl = this.configService.appConfig.appBaseUrl + 'assets/search/' + searchForAsset;
        if (assetTypeForSearch) {
            queryUrl = queryUrl + '?type=' + assetTypeForSearch;
        }
        return this.http.get(queryUrl);
    }

    delinkAsset(delinkAssetDetails) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'assets/deLink', delinkAssetDetails);
    }

    linkAsset(linkAssetDetails) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'assets/link', linkAssetDetails);
    }

    saveMultipleAssetDetails(assetArrayData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'asset/multiple', assetArrayData);
    }

    getAssetlistSpecificList(assetType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByAssetType/' + assetType);
    }
    getAssetDetailsByID(assetID) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'getAssetDetailsById/' + assetID);
    }

    getAssetsByTypeName(assetName){
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByAssetType/' + assetName);
    }
    deleteAssetDetailsByMongodbId(assetMongoDbId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'deleteAssetById/' + assetMongoDbId);
    }

    getAssetLesserByLesserCode(lesserCode) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'lesserByLessorCode/' + lesserCode);
    }

    getAllAssetsForReport(assetTypes) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'assetsByAssetForReport/'+assetTypes);
    }

}


