import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../config.service";

@Injectable()
export class LesserAssociationService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveLesserAssociation(associationDetails) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'lesserAssociation', associationDetails);
    }

    getLesserAssociationCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'lesserAssociation/count');
    }

    getLesserAssociation(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'lesserAssociation' + '/' + skip + '/' + limit);
    }

    getLesserAssociationByCCtype(ccType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'LesserCCTypeDetails' + '/' + ccType);
    }

    getLesserAssociationByMongodbId(CCMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'lesserAssociation/' + CCMongoId);
    }

    updatetLesserAssociationById(CCTransactionId, CCTransactionObj) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'lesserAssociation/' + CCTransactionId, CCTransactionObj);
    }


    deletetLesserAssociationByMongodbId(CCTransactionMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'lesserAssociation/' + CCTransactionMongoId);
    }


    getLesserAssociationByLesserNameAndCCType(lesserName,ccType,vehicleType) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'lesserAssociationByLesserNameAndCCType/' + lesserName+'/'+ccType+'/'+vehicleType);
    }

}
