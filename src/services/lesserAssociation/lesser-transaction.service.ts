import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../config.service";

@Injectable()
export class LesserTransactionService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveLesserTransaction(lesserTransactionData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'lesserTransaction', lesserTransactionData);
    }



}
