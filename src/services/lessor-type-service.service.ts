import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "./config.service";

@Injectable()
export class LessorTypeServiceService {

    constructor(private http: HttpClient, public configService: ConfigService) {
    }

    saveLesserTypeDetails(lesserTypeData) {
        return this.http.post(this.configService.appConfig.appBaseUrl + 'lessorDetails', lesserTypeData);
    }

    getAllLesserType(skip, limit) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'allLessorDetails' + '/' + skip + '/' + limit);
    }

    getLesserTypeByMongodbId(lessorMongoId) {
        return this.http.get(this.configService.appConfig.appBaseUrl + 'lessorBymongoId/' + lessorMongoId);
    }

    updateLesserTypeById(lesserId, lesserTypeData) {
        return this.http.put(this.configService.appConfig.appBaseUrl + 'editLessorBymongoId/' + lesserId, lesserTypeData);
    }


    deleteLesserTypeByMongodbId(lessorMongoId) {
        return this.http.delete(this.configService.appConfig.appBaseUrl + 'lessorBymongoId/' + lessorMongoId);
    }

    getLesserTypeCount() {
        return this.http.get(this.configService.appConfig.appBaseUrl + '/lessorType/count');
    }


}
