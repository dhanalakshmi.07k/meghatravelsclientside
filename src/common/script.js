/**
 * Created by chandru on 29/6/18.
 */
$(document).ready(function () {
  console.log("##@#$#$#%#$%#%#%#")
  let parentEle = $('body');
  fun = {
    popup: function(btn, popup) {
      parentEle.on('click', btn, function(e) {
        e.preventDefault();
        e.stopPropagation();
        // $('.background').fadeIn(250, function() {
        $('.background').fadeIn(250);
        $('.popup').fadeOut(0);
        $(popup).fadeIn(250);
        // });
        $('body,html').scrollTop(0);
      });
      parentEle.on('click', '.popup .close', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.popup').fadeOut(250, function() {
          $('.background').fadeOut(250);
        });
      });
    },
    sliderToggle: function () {
      parentEle.on('click', '#sidebarCollapse', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('#sidebar').toggleClass('active');
      });
      /*$('#sidebarCollapse').on('click', function () {
       $('#sidebar').toggleClass('active');
       });*/
    },
    togglePlusBtn: function () {
      parentEle.on('click', '#addAsset', function(e) {
        e.preventDefault();
        e.stopPropagation();
        $('.assetList').toggle('5000');
        $('#addAssetFabIcon').toggleClass("fa-plus fa-times");
      });
      /*$('#addAsset').click(function() {
       $('.assetList').toggle('5000');
       $('#addAssetFabIcon').toggleClass("fa-plus fa-times");
       });*/
    },
    closeAssetListPopup: function () {
      parentEle.on('click', '#assetListBackBtn', function(e) {
        e.stopPropagation();
        $('#assetListingPopup').slideUp();
      });
    },
    tooltip: function () {
      $('[data-toggle="tooltip"]').tooltip();
    },
    dropDown: function () {
      parentEle.on('click', 'ul#dropdown-item li a', function(e) {
        console.log('!!!!!!!!!!!!!');
        e.preventDefault();
        var concept = $(this).html();
        $('.search-panel span#search_concept').html(concept);
      });
    },
    assetListOpen: function () {
      parentEle.on('click', '#asset-list-plus', function(e) {
        e.stopPropagation();
        $('#assetListingPopup').slideDown();
      });
    }
  };

  fun.popup('.showdDelinkAlertPopupBtn', '.delinkAlertPopup');
  fun.popup('.showLinkAlertPopupBtn', '.linkAlertPopup');
  fun.sliderToggle();
  fun.togglePlusBtn();
  fun.tooltip();
  fun.closeAssetListPopup();
  fun.dropDown();
  fun.assetListOpen();
});
